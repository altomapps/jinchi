module bitbucket.org/altomapps/jinchi

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20161001163130-7b3beb6df3c4
	github.com/fiam/gounidecode v0.0.0-20150629112515-8deddbd03fec
	github.com/golang/protobuf v1.4.2
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/therahulprasad/go-fcm v0.0.0-20160531060939-362576e3ff1c
	github.com/twinj/uuid v0.0.0-20151029044442-89173bcdda19
	go.mongodb.org/mongo-driver v1.3.3
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120
	golang.org/x/sys v0.0.0-20200513112337-417ce2331b5c // indirect
	golang.org/x/text v0.3.3-0.20200513185708-81608d7e9c68 // indirect
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.24.0
)
