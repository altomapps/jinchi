db.quotes.mapReduce(
  // Map function
  function() {

    // We need to save this in a local var as per scoping problems
    var document = this;

    // You need to expand this according to your needs
    var stopwords = ["a","about","above","after","again","against","all","am","an","and","any","are","aren't",
                     "as","at","be","because","been","before","being","below","between","both","but","by","can't",
                     "cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down",
                     "during","each","few","for","from","further","had","hadn't","has","hasn't","have","haven't",
                     "having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself",
                     "his","how","how's","i","i'd","i'll","i'm","i've","if","in","into","is","isn't","it","it's",
                     "its","itself","let's","me","more","most","mustn't","my","myself","no","nor","not","of","off",
                     "on","once","only","or","other","ought","our","ours","ourselves","out","over","own","same",
                     "shan't","she","she'd","she'll","she's","should","shouldn't","so","some","such","than","that",
                     "that's","the","their","theirs","them","themselves","then","there","there's","these","they",
                     "they'd","they'll","they're","they've","this","those","through","to","too","under","until",
                     "up","very","was","wasn't","we","we'd","we'll","we're","we've","were","weren't","what","what's",
                     "when","when's","where","where's","which","while","who","who's","whom","why","why's","with",
                     "won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself",
                     "yourselves"];

    for(var prop in document) {

      // We are only interested in strings and explicitly not in _id
      if(prop !== "quote" && prop !== "reference") {
        continue
      }

      (document[prop]).split(" ").forEach(
          function(w){
              w.split("\n").forEach(
                  function(word){
                      // You might want to adjust this to your needs
                      var cleaned = word.replace(/[#!;,.?-]/g,"")

                      if(
                          // We neither want stopwords...
                          stopwords.indexOf(cleaned.toLowerCase()) > -1 ||
                          // ...nor string which would evaluate to numbers
                          !(isNaN(parseInt(cleaned))) ||
                          !(isNaN(parseFloat(cleaned)))
                      ) {
                          return
                      }
                      emit(cleaned,document._id)
                  })
              })
    }
  },
  // Reduce function
  function(k,v){

    // Kind of ugly, but works.
    // Improvements more than welcome!
    var values = { 'documents': []};
    v.forEach(
      function(vs){
        if(values.documents.indexOf(vs)>-1){
          return
        }
        values.documents.push(vs)
      }
    )
    return values
  },

  {
    // We need this for two reasons...
    finalize:

      function(key,reducedValue){

        // First, we ensure that each resulting document
        // has the documents field in order to unify access
        var finalValue = {documents:[]}

        // Second, we ensure that each document is unique in said field
        if(reducedValue.documents) {

          // We filter the existing documents array
          finalValue.documents = reducedValue.documents.filter(

            function(item,pos,self){

              // The default return value
              var loc = -1;

              for(var i=0;i<self.length;i++){
                // We have to do it this way since indexOf only works with primitives

                if(self[i].valueOf() === item.valueOf()){
                  // We have found the value of the current item...
                  loc = i;
                  //... so we are done for now
                  break
                }
              }

              // If the location we found equals the position of item, they are equal
              // If it isn't equal, we have a duplicate
              return loc === pos;
            }
          );
        } else {
          finalValue.documents.push(reducedValue)
        }
        // We have sanitized our data, now we can return it
        return finalValue

      },
    // Our result are written to a collection called "words"
    out: "words"
  }
)
