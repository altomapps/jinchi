#!/bin/bash

# tools[0]="glide"                                                type[0]="cmd";  cmd[0]="curl https://glide.sh/get | sh"
tools[0]="protoc-gen-go";                                       type[1]="cmd";  cmd[1]="go get -u github.com/golang/protobuf/protoc-gen-go"
tools[1]="gocode";                                              type[2]="cmd";  cmd[2]="gocode close; go get -u github.com/nsf/gocode"

echo "Checking dependencies:"
for i in ${!tools[@]}; do
    TOOL=""
    case ${type[$i]} in
        "cmd")
            TOOL=$(command -v ${tools[$i]})
        ;;
        "dir")
            if [ -d ${tools[$i]} ]; then
                TOOL="grpc"
            else
                TOOL=""
            fi
        ;;
    esac

    if [ -z "$TOOL" ] && [ "${TOOL+xxx}" = "xxx" ]; then
        echo " * Retrieving: ${tools[$i]}"
    else
        echo " * Updating: ${tools[$i]}"
    fi
    eval ${cmd[$i]}
done
