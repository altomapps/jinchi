#!/bin/bash

mkdir -p installer/payload
mv jinchi_*_*.tar.bz2 installer/payload
cp scripts/installer/install/* installer/payload
cp scripts/installer/decompress/decompress.sh installer

cd installer/payload
tar cf ../payload.tar ./*
cd ..

version=$(git describe --long --tags --dirty --always)
if [ -e "payload.tar" ]; then
    gzip payload.tar

    if [ -e "payload.tar.gz" ]; then
        cat decompress.sh payload.tar.gz > jinchi-${version}.bsx
        mv jinchi-${version}.bsx ../
        cd ../
        chmod +x jinchi-${version}.bsx
        rm -rf installer
    else
        echo "payload.tar.gz does not exist"
        exit 1
    fi
else
    echo "payload.tar does not exist"
    exit 1
fi

echo "jinchi-${version}.bsx created"
exit 0
