#!/bin/bash

if [ $# -lt 2 ]
then
    exit 1
fi

fileName=$2
folder=$1
backupFolder=${folder}-backup-$(date +"%s")

# Switch to user jinchi
exec sudo -u jinchi /bin/sh - << eof

echo " - Copying archive..."
cp ${fileName} ~jinchi/
cd ~jinchi
echo " - Backing up old installation..."
mv ${folder} ${backupFolder}
tar cjf ${backupFolder}.tar.bz2 ${backupFolder}

echo " - Extracting new version"
mkdir -p ${folder}/log
tar xf ${fileName} -C ${folder}
cp -r ${backupFolder}/keys ${folder}
chmod +x ${folder}/jinchi

eof
