#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
elif [ $# -lt 1 ]; then
    echo "Please specify service" 1>&2
    exit 1
fi

ARCHIVE_NAME=`ls jinchi_*_*.tar.bz2`

service $1 stop

echo "Installing the new archive"
echo $(pwd)
chmod 777 .
chown jinchi:jinchi ${ARCHIVE_NAME}
./unpack_files.sh $1 ${ARCHIVE_NAME}

service $1 start
