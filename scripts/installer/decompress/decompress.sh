#!/bin/bash

echo ""
echo "Self Extracting Installer"
echo ""


if [ $# -lt 1 ]
then
    echo "Please specify service" 1>&2
    exit 1
fi

export TMPDIR=`mktemp -d /tmp/selfextract.XXXXXX`

ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0`

tail -n+$ARCHIVE $0 | tar xzv -C $TMPDIR

CDIR=`pwd`
cd $TMPDIR
sudo ./install.sh $1

cd $CDIR
rm -rf $TMPDIR

exit 0

__ARCHIVE_BELOW__
