#!/bin/bash
openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "/C=UK/ST=Hampshire/L=Southampton/O=Dis/CN=localhost" -keyout keys/server.key -out keys/server.pem
