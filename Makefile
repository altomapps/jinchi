# See README.txt.

.PHONY: all release heroku glide_update update_submodules update_tools test tags archive_release win64 linux64 linux_arm linux_arm64 mac64

endpoint_protos := $(wildcard protobuf/endpoints/*.proto)
error_protos := $(wildcard protobuf/error/*.proto)
proto_compiled := $(wildcard protobuf/*/*.pb.go)
go_files := $(*/*/%.go)

GOCMD=go
VERSION=`git describe --long --tags --dirty --always`
BUILD_TIME=`date +%FT%T%z`
HOST=`hostname`
USER=`whoami`

# If the system is linux try using the protobuf compiler included in the repository
ifeq (`uname`,Linux)
 PROTOC=./protobuf/bin/protoc
else
 PROTOC?=$(GOPATH)/bin/protoc
endif


# Setup the -ldflags option for go build here, interpolate the variable values
LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.BuildTime=${BUILD_TIME} -X main.BuildBy=${USER}@${HOST} -w"

all: jinchi
release: jinchi_release

jinchi: update_tools update_submodules protobuf_cmpl jinchi_go
jinchi_release: protobuf_cmpl jinchi_go_release
prep: update_tools protobuf_cmpl tags

clean:
	@echo "Cleaning up..."
	@echo -e "\t Protocol Buffers"
	@rm -f protobuf_cmpl
	@rm -f $(proto_compiled)
	@echo -e "\t Jinchi"
	@rm -f jinchi
	@rm -f jinchi.exe
	@rm -f jinchi_*_*.tar.bz2
	@rm -f *.bsx
	@${GOCMD} clean -v ./...

# TODO: This could also have godep restore (or something don't remember the exact syntax to install the required
# 	    libraries in your $GOPATH)
update_tools:
	@scripts/dependencies.sh

protobuf_cmpl: $(endpoint_protos) $(error_protos)
	@echo "Compiling endpoints: "
	@- $(foreach proto,$(endpoint_protos), echo -e "\t"$(proto); )
	@${PROTOC} --proto_path=protobuf/ --go_out=plugins=grpc:protobuf/ $(endpoint_protos)
	@touch protobuf_cmpl

jinchi_go_release: $(go_files) protobuf_cmpl
	@echo "Building Jinchi..."
	@${GOCMD} build ${LDFLAGS} bitbucket.org/altomapps/jinchi/cmd/jinchi

jinchi_go: $(go_files) protobuf_cmpl
	@echo "Building Jinchi with race detection ON..."
	@${GOCMD} build -race ${LDFLAGS} bitbucket.org/altomapps/jinchi/cmd/jinchi

test: protobuf_cmpl
	@${GOCMD} test -race ./... -v

# #####################
# go related targets
# #####################

glide_update:
	@echo "Installing local dependencies..."
	@glide -q install
	@glide cc # Clear glide cache
	@glide up # Update dependencies

update_libraries:
	@echo "Updating libraries..."
	@${GOCMD} get -u ./...

# ###################
# Git related targets
# ###################
heroku:
	@echo "Pushing to heroku... "
	@git push heroku master

update_submodules:
	@echo "Updating submodules..."
	@git submodule foreach git pull origin master

tags:
	@echo "Generating Tags..."
	@gotags -tag-relative=true -R=true -sort=true -f="tags" -fields=+l .
	@echo "Building project..."
	@${GOCMD} install ./cmd/jinchi
	@echo "Setting gocode autobuild to true..."
	@gocode set autobuild true

# #######################
# Cross Comile releases
# ######################

archive_release:
	@echo "Archiving..."; \
	tar -cvjf jinchi_$(GOOS)_$(GOARCH).tar.bz2 $(EXE) config; \
	rm $(EXE); \
	scripts/installer/builder/builder.sh

win64: export GOOS=windows
win64: export GOARCH=amd64
win64: export EXE=jinchi.exe
win64: jinchi_release archive_release

linux64: export GOOS=linux
linux64: export GOARCH=amd64
linux64: export EXE=jinchi
linux64: jinchi_release archive_release

linux_arm: export GOOS=linux
linux_arm: export GOARCH=arm
linux_arm: export GOARM=6
linux_arm: export EXE=jinchi
linux_arm: jinchi_release archive_release

linux_arm64: export GOOS=linux
linux_arm64: export GOARCH=arm64
linux_arm64: export GOARM=7
linux_arm64: export EXE=jinchi
linux_arm64: jinchi_release archive_release

mac64: export GOOS=darwin
mac64: export GOARCH=amd64
mac64: export EXE=jinchi
mac64: jinchi_release archive_release
