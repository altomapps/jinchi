package schema

import "go.mongodb.org/mongo-driver/bson/primitive"

// Quote entry
type Quote struct {
	Id        primitive.ObjectID `bson:"_id,omitempty"`
	Category  primitive.ObjectID
	Quote     string
	Reference string
	Img       string
}

// Category entry
type Category struct {
	Id          primitive.ObjectID `bson:"_id,omitempty"`
	Name        string
	Description string
	Img         string
}

type Suggestion struct {
	Id string `bson:"_id"`
}
