package schema

import (
	"time"

	"bitbucket.org/altomapps/jinchi/protobuf/endpoints"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ArigatouList struct {
	date   time.Time          `bson:"date"`
	userID primitive.ObjectID `bson:"userID"`
}

// UserProfileMetaArigatou arigatou metadata
type UserProfileMetaArigatou struct {
	SentCounter     uint64         `bson:"sent_counter"`
	ReceivedCounter uint64         `bson:"received_counter"`
	Sent            []ArigatouList `bson:"sent"`
	Received        []ArigatouList `bson:"received"`
}

// UserProfileMeta entry.
type UserProfileMeta struct {
	Attack       uint64                  `bson:"attack_count"`
	Received     uint64                  `bson:"received_count"`
	CreationDate time.Time               `bson:"creation_date"`
	LastReceived time.Time               `bson:"last_received"`
	NextAttack   time.Time               `bson:"next_attack"`
	Arigatou     UserProfileMetaArigatou `bson:"arigatou"`
}

// ReceivedList holds a single record for a received item
type ReceivedList struct {
	Quote      Quote              `bson:"quote"`
	SenderID   primitive.ObjectID `bson:"sender_id"`
	SenderName string             `bson:"sender_name"`
	Date       time.Time          `bson:"date"`
	Arigatou   time.Time          `bson:"arigatou"`
}

// UserLists entry for a user
type UserLists struct {
	Sent       []primitive.ObjectID `bson:"sent"`
	Favourites []primitive.ObjectID `bson:"favourites"`
	Hidden     []primitive.ObjectID `bson:"hidden"`
	Received   []ReceivedList       `bson:"received"`
}

// Device registration entry for push notifications
type Device struct {
	DeviceId string `bson:"device_id"`
	Token    string `bson:"token"`
	Version  string `bson:"version"`
}

// UserProfile entry
type UserProfile struct {
	ID        primitive.ObjectID  `bson:"_id,omitempty"`
	UserName  string              `bson:"user_name"`
	Name      string              `bson:"name"`
	Email     string              `bson:"email"`
	Password  string              `bson:"password"`
	LoginType endpoints.LoginType `bson:"login_type"`
	Image     string              `bson:"image"`
	NinjaName string              `bson:"ninja_name"`

	Devices []Device        `bson:"devices"`
	Meta    UserProfileMeta `bson:"meta"`
	Lists   UserLists       `bson:"lists"`
}

// Session entry for storring active sessions
type Session struct {
	ID           primitive.ObjectID `bson:"_id,omitempty"`
	SessionId    string             `bson:"session_id"`
	UserId       primitive.ObjectID `bson:"user_id"`
	NinjaName    string             `bson:"ninja_name"`
	SessionType  string             `bson:"session_type"`
	Lists        UserLists          `bson:"lists"`
	LastActivity time.Time          `bson:"last_activity"`
}
