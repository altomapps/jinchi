package schema

import "go.mongodb.org/mongo-driver/bson/primitive"

// Motd holds a record for a week of quotes o fthe day
type Motd struct {
	Week      int                  `bson:"week"`
	Author    string               `bson:"author"`
	AuthorImg string               `bson:"author_img"`
	QuoteIDs  []primitive.ObjectID `bson:"quote_ids"`
}
