package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"google.golang.org/grpc/grpclog"

	gcm "github.com/therahulprasad/go-fcm"

	"bitbucket.org/altomapps/jinchi/lib/schema"
	pb "bitbucket.org/altomapps/jinchi/protobuf/endpoints"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// GetMotd retrieves the list of quotes to be sent this week
func (r *MongoRepository) GetMotd() *schema.Motd {
	var motd schema.Motd
	// session := r.Session.Copy()
	// defer session.Close()

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	col := r.Client.Database(r.DatabaseName).Collection(r.MotdCollection)
	now := time.Now()
	_, weekNo := now.ISOWeek()
	weekYear := concatinateStrings(strconv.Itoa(now.Year()), "-", strconv.Itoa(weekNo))

	week := bson.M{"week": weekYear}

	err := col.FindOne(ctx, week).Decode(&motd)
	if err != nil {
		// Try the old method just in case
		week = bson.M{"week": weekNo}
		err := col.FindOne(ctx, week).Decode(&motd)

		if err != nil {
			log.Fatalf("Couldn't retrieve Week(%s): %s", weekYear, err.Error())
		}
	}

	return &motd
}

// GetQuoteOfTheDay retrieves the quote to be sent today based on the ID provided.
func (r *MongoRepository) GetQuoteOfTheDay(motd *schema.Motd) *schema.Quote {
	var quote schema.Quote

	// session := r.Session.Copy()
	// defer session.Close()

	if motd == nil {
		motd = r.GetMotd()
	}

	var day int
	switch time.Now().Weekday() {
	case time.Monday:
		day = 0
	case time.Tuesday:
		day = 1
	case time.Wednesday:
		day = 2
	case time.Thursday:
		day = 3
	case time.Friday:
		day = 4
	case time.Saturday:
		day = 5
	case time.Sunday:
		day = 6
	}
	quoteID := motd.QuoteIDs[day]

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	col := r.Client.Database(r.DatabaseName).Collection(r.QuoteCollection)
	err := col.FindOne(ctx, bson.M{"_id": quoteID}).Decode(&quote)
	if err != nil {
		log.Fatalf("Couldn't retrieve quote for day %d: %s", day, err.Error())
	}
	return &quote
}

// GetActiveDevices retrieves all devices that have been used in the last 4 weeks
func (r *MongoRepository) GetActiveDevices() []schema.UserProfile {
	var users []schema.UserProfile

	// session := r.Session.Copy()
	// defer session.Close()

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	col := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)

	opts := options.Find().SetProjection(bson.M{"_id": 1, "devices": 1})
	iter, err := col.Find(ctx, bson.M{"devices.0": bson.M{"$exists": true}}, opts)
	if err != nil {
		log.Fatalf("Error retrieving devices: %s \n", err.Error())
	}

	iter.All(ctx, &users)

	return users
}

// SendMotd Retrieves the quote of the day from the database and sends it to all active users.
func (r *MongoRepository) SendMotd(quoteOfTheDay *primitive.ObjectID) {
	var (
		quote *schema.Quote
		motd  *schema.Motd
	)
	// Create a Sender to send the message.
	sender := &gcm.Sender{ApiKey: r.googleConfig.APIKey}

	if quoteOfTheDay == nil {
		motd = r.GetMotd()
		quote = r.GetQuoteOfTheDay(motd)
	} else {
		quote = &schema.Quote{}
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		col := r.Client.Database(r.DatabaseName).Collection(r.QuoteCollection)
		err := col.FindOne(ctx, bson.M{"_id": quoteOfTheDay}).Decode(quote)
		if err != nil {
			fmt.Printf("Couldn't retrieve quote(%s): %s", quoteOfTheDay.Hex(), err.Error())
			log.Fatalf("Couldn't retrieve quote(%s): %s", quoteOfTheDay.Hex(), err.Error())
		}
		motd = &schema.Motd{}
	}

	users := r.GetActiveDevices()
	fmt.Println(quote)
	data := quoteToMapBasic(quote, motd.AuthorImg, "motd")

	log.Println("Sending Quote of the day =========================")
	prettyQuote, _ := json.MarshalIndent(data, "Notification: ", "  ")
	log.Println(string(prettyQuote))
	var (
		deviceTokens  []string
		quoteChannels = make([]chan *schema.ReceivedList, len(users))
	)

	for userIndex, user := range users {
		quoteChannels[userIndex] = make(chan *schema.ReceivedList)
		for devIndex, device := range user.Devices {
			deviceTokens = append(deviceTokens, device.Token)
			if (len(deviceTokens) == 1000) || ((userIndex == len(users)-1) && (devIndex == len(user.Devices)-1)) {
				msg := gcm.NewMessage(data, deviceTokens...)
				response, err := sender.Send(msg, 2)
				if err != nil {
					prettyError, _ := json.MarshalIndent(err, "GCMError: ", "  ")
					log.Println(string(prettyError))
				} else {
					prettyResponse, _ := json.MarshalIndent(response, "Response: ", "  ")
					log.Println(string(prettyResponse))

					var devicesToRemove []bson.M

					for index, result := range response.Results {
						if strings.EqualFold(result.Error, "NotRegistered") {
							devicesToRemove = append(devicesToRemove, bson.M{"token": deviceTokens[index]})
						}
					}
					if len(devicesToRemove) > 0 {
						r.RemoveExpiredTokens(devicesToRemove, nil)

						prettyDevicesToRemove, _ := json.MarshalIndent(devicesToRemove, "UnregisteredTokens: ", "  ")
						log.Println(string(prettyDevicesToRemove))
					}
				}
				deviceTokens = nil
			}
		}
		go r.addQuoteToUserList(pb.ListType_RECEIVED, user.ID, quoteChannels[userIndex])
	}
	r.broadcastQuoteToListeners(quote.Id, primitive.NewObjectID(), "the Ninja", quoteChannels)
	log.Println("Finished =========================================")
}

// Close the active mongo session
func (r *MongoRepository) Close() {
	// ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	// r.Client.Close(ctx)
}

// RemoveExpiredTokens deletes the provided device token records from the database as they are not valid anymore
func (r *MongoRepository) RemoveExpiredTokens(devicesToRemove []bson.M, userSession *schema.Session) {
	var sessionIDString string
	// session := r.Session.Copy()
	// defer session.Close()

	if userSession != nil {
		sessionIDString = concatinateStrings("[", userSession.SessionId, "] - ")
	}

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	userCollection := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)
	opts := options.Update().SetUpsert(true)

	for _, device := range devicesToRemove {

		info, err := userCollection.UpdateMany(ctx, bson.M{}, bson.M{"$pull": bson.M{"devices": device}}, opts)
		if err != nil {

			grpclog.Printf("%sRemoveExpiredTokens: Failed to remove device[%s] with error %v\n",
				sessionIDString, device, err)
		} else {
			prettyInfo, _ := json.Marshal(info)
			grpclog.Printf("%sRemoveExpiredTokens: Removed device[%s]: %s\n", sessionIDString, device,
				string(prettyInfo))
		}
	}
}
