package repository

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/grpclog"

	gcm "github.com/therahulprasad/go-fcm"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	"bitbucket.org/altomapps/jinchi/lib/ninja"
	"bitbucket.org/altomapps/jinchi/lib/schema"
	pb "bitbucket.org/altomapps/jinchi/protobuf/endpoints"
)

/* Utility functions */
func (r *MongoRepository) retrieveCategoryQuotes(request *pb.QuoteCategoryRequest, quoteChannel chan quoteChan,
	userSession *schema.Session) {
	var (
		query      bson.M
		categoryID bson.M
		excludes   bson.M
		quote      = new(schema.Quote)
		finalQuery = make([]bson.M, 0)
	)

	defer close(quoteChannel)

	ctx, _ := context.WithTimeout(context.Background(), 60*time.Second)
	quotesCollection := r.Client.Database(r.DatabaseName).Collection(r.QuoteCollection)
	if bytes.Compare(request.Category, []byte("************")) != 0 {
		objectID, _ := primitive.ObjectIDFromHex(hex.EncodeToString(request.Category))
		categoryID = bson.M{"category": objectID}
		finalQuery = append(finalQuery, categoryID)
	}
	if (len(userSession.Lists.Hidden) > 0) || (len(userSession.Lists.Sent) > 0) {
		var quotesToExclude []primitive.ObjectID
		quotesToExclude = append(quotesToExclude, userSession.Lists.Hidden...)
		quotesToExclude = append(quotesToExclude, userSession.Lists.Sent...)
		excludes = bson.M{"_id": bson.M{"$nin": quotesToExclude}}
		finalQuery = append(finalQuery, excludes)
	}

	if len(request.LastQuoteId) > 0 || request.Offset == 0 {
		var skip bson.M
		lastQuoteID := hex.EncodeToString(request.LastQuoteId)

		objectID, err := primitive.ObjectIDFromHex(lastQuoteID)
		if err == nil {
			fmt.Printf("retrieveCategoryQuotes: %v\n", err)
			skip = bson.M{"_id": bson.M{"$lt": objectID}}
			finalQuery = append(finalQuery, skip)
		}

		// query = bson.M{"$and": []bson.M{categoryID, skip, excludes}}
		query = bson.M{"$and": finalQuery}

		opts := options.Find().SetSort(bsonx.Doc{{"_id", bsonx.Int32(-1)}}).SetBatchSize(int32(request.Limit))
		iter, err := quotesCollection.Find(ctx, query, opts)
		if err != nil {
			quoteChannel <- quoteChan{quote, grpc.Errorf(codes.Internal, "%v", err)}
			return
		}

		defer iter.Close(ctx)
		for iter.Next(ctx) {
			iter.Decode(quote)
			quoteChannel <- quoteChan{quote, nil}
			quote = new(schema.Quote)
		}
		return
	}

	// get number of Quotes in this category.
	categoryLength, err := quotesCollection.CountDocuments(ctx, categoryID)

	if err != nil {
		quoteChannel <- quoteChan{quote, grpc.Errorf(codes.Internal, "%v", err)}
		return
	}
	if uint32(categoryLength) > request.Offset {
		// Adust the limit
		limit := adjustLimit(request.Offset, request.Limit, uint32(categoryLength))
		opts := options.Find().SetSort(bsonx.Doc{{"_id", bsonx.Int32(-1)}}).SetSkip(int64(request.Offset)).SetLimit(int64(limit))
		// TODO: Check if that is still used and get rid of it if it isn't
		// TODO: Remove in APRIL 2017 at the latest?
		cursor, err := quotesCollection.Find(ctx, categoryID, opts)
		defer cursor.Close(ctx)

		if err != nil {
			quoteChannel <- quoteChan{quote, grpc.Errorf(codes.Internal, "%v", err)}
			return
		}
		// Retrieve the requested list of quotes
		for cursor.Next(context.Background()) {
			cursor.Decode(quote)
			quoteChannel <- quoteChan{quote, nil}
			quote = new(schema.Quote)
		}
	}
}

func (r *MongoRepository) retrieveListQuotes(list *[]primitive.ObjectID, lastQuoteIDHex []byte, limit uint32, quoteChannel chan quoteChan) {
	var (
		skip bson.M
		// session = r.Session.Copy()
		quote      = new(schema.Quote)
		finalquery []bson.M
	)

	// defer session.Close()
	defer close(quoteChannel)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	quotesCollection := r.Client.Database(r.DatabaseName).Collection(r.QuoteCollection)

	lastQuoteID := hex.EncodeToString(lastQuoteIDHex)

	objectID, err := primitive.ObjectIDFromHex(lastQuoteID)
	if err == nil {
		skip = bson.M{"_id": bson.M{"$lt": objectID}}
		finalquery = append(finalquery, skip)
	}

	quotesList := bson.M{"_id": bson.M{"$in": list}}
	finalquery = append(finalquery, quotesList)
	query := bson.M{"$and": finalquery}
	opts := options.Find().SetSort(bsonx.Doc{{"_id", bsonx.Int32(-1)}}).SetLimit(int64(limit))
	iter, err := quotesCollection.Find(ctx, query, opts)

	if err != nil {
		quoteChannel <- quoteChan{quote, grpc.Errorf(codes.Internal, "%v", err)}
		return
	}

	defer iter.Close(ctx)

	for iter.Next(ctx) {
		iter.Decode(quote)
		quoteChannel <- quoteChan{quote, nil}
		quote = new(schema.Quote)
	}

	return
}

func (r *MongoRepository) retrieveSearchQuotes(request *pb.SearchQuotesRequest, quoteChannel chan quoteChan,
	userSession *schema.Session) {
	var (
		// session = r.Session.Copy()
		quote = new(schema.Quote)
	)
	// defer session.Close()
	defer close(quoteChannel)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	opts := options.Find().SetProjection(bson.M{
		"score": bson.M{"$meta": "textScore"},
	}).SetSort("$textScore:score")
	iter, _ := r.Client.Database(r.DatabaseName).Collection(r.QuoteCollection).Find(ctx, bson.M{
		"$text": bson.M{"$search": request.SearchTerms},
	}, opts)

	defer iter.Close(ctx)
	for iter.Next(ctx) {
		iter.Decode(quote)
		quoteChannel <- quoteChan{quote, nil}
		quote = new(schema.Quote)
	}

	return
}

func (r *MongoRepository) typeAheadSuggestions(request *pb.QuoteTypeAheadRequest, suggestionChannel chan suggestionChan) {
	var (
		suggestion = new(schema.Suggestion)
	)
	// defer session.Close()
	defer close(suggestionChannel)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	opts := options.Find().SetProjection(bson.M{"_id": 1})
	iter, _ := r.Client.Database(r.DatabaseName).Collection(r.WordCollection).Find(ctx,
		bson.M{"_id": primitive.Regex{Pattern: concatinateStrings("^", request.Term), Options: ""}}, opts)

	defer iter.Close(ctx)

	for iter.Next(ctx) {
		iter.Decode(suggestion)
		suggestionChannel <- suggestionChan{suggestion, nil}
		suggestion = new(schema.Suggestion)
	}
	return
}

func (r *MongoRepository) increaseUserCounter(metaName []string, userID, affectedUserID primitive.ObjectID) error {
	metaCounters := bson.M{}
	metaLists := bson.M{}
	for _, meta := range metaName {
		metaCounters[concatinateStrings("meta.", meta, "_counter")] = 1
		metaLists[concatinateStrings("meta.", meta)] = bson.M{"userID": affectedUserID, "date": time.Now()}
	}

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	opts := options.FindOneAndUpdate().SetReturnDocument(options.After)
	result := r.Client.Database(r.DatabaseName).Collection(r.UserCollection).FindOneAndUpdate(ctx, bson.M{"_id": userID},
		bson.M{"$inc": metaCounters, "$addToSet": metaLists}, opts)

	if result.Err() != nil {
		return grpc.Errorf(codes.Canceled, "Error while updating user counters: %s", result.Err().Error())
	}

	return nil
}

func (r *MongoRepository) updateArigatouDate(quote, senderID primitive.ObjectID, userSession *schema.Session) {
	quoteID := quote
	// session := r.Session.Copy()
	// defer session.Close()

	if len(userSession.Lists.Received) == 0 {
		return
	}

	// if we didn't receive a QuoteID with the arigatou assume it's for the latest quote.
	if quoteID == r.GetTempQuoteID() {
		quoteID = userSession.Lists.Received[len(userSession.Lists.Received)-1].Quote.Id
	}

	grpclog.Printf("[%s] - updateArigatouDate: Quote=%v, Sender=%v, Date: %v\n", userSession.SessionId, quoteID, senderID,
		time.Now())

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	// Ignoring return value for now as there is nothing to do with it!
	r.Client.Database(r.DatabaseName).Collection(r.UserCollection).UpdateMany(ctx,
		bson.M{"_id": userSession.UserId,
			"lists.received": bson.M{"$elemMatch": bson.M{"quote._id": quoteID, "sender_id": senderID}}},
		bson.M{"$set": bson.M{"lists.received.$.arigatou": time.Now()}},
	)

	r.Client.Database(r.DatabaseName).Collection(r.SessionCollection).UpdateMany(ctx,
		bson.M{"session_id": userSession.SessionId,
			"lists.received": bson.M{"$elemMatch": bson.M{"quote._id": quoteID, "sender_id": senderID}}},
		bson.M{"$set": bson.M{"lists.received.$.arigatou": time.Now()}},
	)
}

func (r *MongoRepository) addQuoteToUserList(list pb.ListType, userID primitive.ObjectID,
	quoteChannel chan *schema.ReceivedList) error {
	// session := r.Session.Copy()
	// defer session.Close()

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	userCollection := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)
	sessionCollection := r.Client.Database(r.DatabaseName).Collection(r.SessionCollection)
	quoteListElement := <-quoteChannel

	userList := strings.ToLower(concatinateStrings("lists.", pb.ListType_name[int32(list)]))

	// Remove quotes received more than 7 days ago.
	go func() {
		// session := r.Session.Copy()
		// defer session.Close()
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		userCollectionClean := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)
		sessionCollectionClean := r.Client.Database(r.DatabaseName).Collection(r.SessionCollection)

		oldQuotes := bson.M{userList: bson.M{"date": bson.M{"$lt": time.Now().Add(-(30 * 24) * time.Hour)}}}

		_, err := userCollectionClean.UpdateOne(ctx, bson.M{"_id": userID}, bson.M{"$pull": oldQuotes})
		if err != nil {
			grpclog.Printf("addQuoteToUserList: cleanup user: %v\n", err)
			grpclog.Printf("Error while updating user list(%s): %v", pb.ListType_name[int32(list)], err)
		}

		_, err = sessionCollectionClean.UpdateOne(ctx, bson.M{"user_id": userID}, bson.M{"$pull": oldQuotes})
		if err != nil {
			grpclog.Printf("addQuoteToUserList: cleanup session: %v\n", err)
			grpclog.Printf("Error while updating session list(%s): %v", pb.ListType_name[int32(list)], err)
		}
	}()

	_, err := userCollection.UpdateOne(ctx, bson.M{"_id": userID}, bson.M{"$push": bson.M{userList: *quoteListElement}})
	if err != nil {
		grpclog.Printf("addQuoteToUserList: update User: %v\n", err)
		return grpc.Errorf(codes.Internal, "updateSender: %v", err)
	}

	_, err = sessionCollection.UpdateOne(ctx, bson.M{"user_id": userID}, bson.M{"$push": bson.M{userList: quoteListElement}})
	if err != nil {
		grpclog.Printf("addQuoteToUserList: update session: %v\n", err)
		return grpc.Errorf(codes.Internal, "Could not update user session record %v", err)
	}

	grpclog.Printf("addQuoteToUserList: Finished updating User: %v\n", userID)
	return nil
}

func (r *MongoRepository) updateSender(quoteID primitive.ObjectID, userSession *schema.Session) error {
	// if session == nil {
	// 	session = r.Session.Copy()
	// 	defer session.Close()
	// }

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	userCollection := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)
	sessionCollection := r.Client.Database(r.DatabaseName).Collection(r.SessionCollection)
	sentList := strings.ToLower(concatinateStrings("lists.", pb.ListType_name[int32(pb.ListType_SENT)]))
	_, err := userCollection.UpdateOne(ctx, bson.M{"_id": userSession.UserId},
		bson.M{
			"$inc":      bson.M{"meta.attack_count": 1},
			"$set":      bson.M{"meta.next_attack": time.Now().Add(time.Hour)},
			"$addToSet": bson.M{sentList: quoteID},
		})
	if err != nil {
		err = grpc.Errorf(codes.Internal, "updateSender: %v", err)
	}

	_, err = sessionCollection.UpdateOne(ctx, bson.M{"_id": userSession.ID},
		bson.M{"$addToSet": bson.M{sentList: quoteID}})
	if err != nil {
		return grpc.Errorf(codes.Internal, "Could not update user session record %v", err)
	}

	// Trim user sent quotes list down to 200 items
	_, err = userCollection.UpdateOne(ctx, bson.M{"_id": userSession.UserId},
		bson.M{
			"$push": bson.M{
				sentList: bson.M{
					"$each":  []bson.M{},
					"$sort":  -1,
					"$slice": 200,
				},
			},
		})
	if err != nil {
		err = grpc.Errorf(codes.Internal, "Error while trimming sent list: %v", err)
	}

	_, err = sessionCollection.UpdateOne(ctx, bson.M{"_id": userSession.ID},
		bson.M{
			"$push": bson.M{
				sentList: bson.M{
					"$each":  []bson.M{},
					"$sort":  -1,
					"$slice": 200,
				},
			},
		})
	if err != nil {
		err = grpc.Errorf(codes.Internal, "Error while trimming sent list: %v", err)
	}

	return err
}

func (r *MongoRepository) retrieveQuote(quoteID primitive.ObjectID) chan quoteChan {
	quoteChannel := make(chan quoteChan)

	go func(quoteID primitive.ObjectID) {
		var quote schema.Quote

		defer close(quoteChannel)

		// Retrieve the quote to send
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		result := r.Client.Database(r.DatabaseName).Collection(r.QuoteCollection).FindOne(ctx, bson.M{"_id": quoteID})
		if result.Err() != nil {
			quoteChannel <- quoteChan{nil, grpc.Errorf(codes.Internal, "retrieveQuote: %v", result.Err())}
			return
		}
		result.Decode(&quote)
		quoteChannel <- quoteChan{&quote, nil}
		return
	}(quoteID)

	return quoteChannel
}

func (r *MongoRepository) broadcastQuoteToListeners(quoteID, senderID primitive.ObjectID,
	senderNinjaName string, listeners []chan *schema.ReceivedList) {
	source := r.retrieveQuote(quoteID)
	go func() {
		pbQuote := <-source
		if pbQuote.err != nil {
			return //pbQuote.err
		}

		quoteListElement := schema.ReceivedList{
			Quote:      *(pbQuote.quote),
			SenderID:   senderID,
			SenderName: senderNinjaName,
			Date:       time.Now(),
			Arigatou:   time.Time{},
		}

		for _, listener := range listeners {
			go func(listener chan *schema.ReceivedList, quoteListElement schema.ReceivedList) {
				listener <- &quoteListElement
				close(listener)
			}(listener, quoteListElement)
		}
	}()
}

func (r *MongoRepository) retrieveSendUsers(userSession *schema.Session) chan userChan {
	userChannel := make(chan userChan)

	go func(userSession *schema.Session) {
		var lastReceived bson.M
		defer close(userChannel)

		// Select The users to send the quote to
		users := []schema.UserProfile{}
		collection := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)

		// TODO: Use the following query with MongoDB 3.2+
		/*
			query := session.DB(r.DatabaseName).C(r.userCollection).Pipe([]bson.M{
				bson.M{"$match": bson.M{"$and": []bson.M{
					bson.M{"_id": bson.M{"$ne": userID}},
					bson.M{"meta.last_received": bson.M{"$lte": time.Now().Add(-6 * time.Hour)}},
				}}},
				bson.M{"$sample": bson.M{"size": 8}},
			})
		*/

		// If there are no results relax the query to include users that haven't received anything in the last half an hour
		//	if (err == nil) && (len(users) == 0) {

		if r.environment == EnvDev {
			lastReceived = bson.M{}
		} else {
			lastReceived = bson.M{"meta.last_received": bson.M{"$lte": time.Now().Add(-1 * time.Hour)}}
		}

		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		opts := options.Find().SetSort(bsonx.Doc{{"meta.last_received", bsonx.Int32(1)}}).SetLimit(8).SetProjection(bson.M{"_id": 1, "devices": 1})
		query, err1 := collection.Find(ctx, bson.M{"$and": []bson.M{
			bson.M{"_id": bson.M{"$ne": userSession.UserId}},
			bson.M{"devices.0": bson.M{"$exists": true}},
			lastReceived,
		}}, opts)
		defer query.Close(ctx)
		if err1 != nil {
			fmt.Println(err1)
		}
		err := query.All(context.TODO(), &users)
		//	}

		if err != nil {
			userChannel <- userChan{nil, grpc.Errorf(codes.Internal, "retrieveSendUsers: %v", err)}
			return
		}

		userChannel <- userChan{users, nil}

		prettyUsers, _ := json.Marshal(users)
		grpclog.Printf("[%s] - retrieveSendUsers: : %s\n", userSession.SessionId, string(prettyUsers))
		fmt.Println(string(prettyUsers))
		return
	}(userSession)
	return userChannel
}

func (r *MongoRepository) getUserDevices(senderID primitive.ObjectID) ([]string, error) {
	var (
		user    schema.UserProfile
		devices []string
	)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result := r.Client.Database(r.DatabaseName).Collection(r.UserCollection).FindOne(ctx, bson.M{"_id": senderID})
	if result.Err() != nil {
		return nil, grpc.Errorf(codes.Internal, "Couldn't retrieve Sender's devices(%s)", result.Err().Error())
	}
	result.Decode(&user)

	for _, dev := range user.Devices {
		devices = append(devices, dev.Token)
	}

	return devices, grpc.Errorf(codes.OK, "")
}

func (r *MongoRepository) retrieveDevices(userChannel chan userChan, quoteID primitive.ObjectID,
	senderID primitive.ObjectID, senderNinjaName string) chan deviceChan {
	deviceChannel := make(chan deviceChan)

	go func(userChannel chan userChan) {
		var wg sync.WaitGroup
		defer close(deviceChannel)

		// Retrive list of users
		userResp := <-userChannel
		if userResp.err != nil {
			deviceChannel <- deviceChan{nil, userResp.err}
			return
		}

		devices := []string{}
		quoteChannels := make([]chan *schema.ReceivedList, len(userResp.users))

		for i, selectedUser := range userResp.users {
			quoteChannels[i] = make(chan *schema.ReceivedList)
			wg.Add(1)
			go func(user schema.UserProfile, counter int) {
				defer wg.Done()
				userCollection := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)

				ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
				_, err := userCollection.UpdateOne(ctx, bson.M{"_id": user.ID},
					bson.M{
						"$inc": bson.M{"meta.received_count": 1},
						"$set": bson.M{"meta.last_received": time.Now()},
					})

				if err != nil {
					grpclog.Printf("\t\t retrieveDevices: Failed to update user's [%s] meta data (%v)",
						user.ID.Hex(), err)
				}
				go r.addQuoteToUserList(pb.ListType_RECEIVED, user.ID, quoteChannels[counter])
			}(selectedUser, i)

			for _, dev := range selectedUser.Devices {
				devices = append(devices, dev.Token)
			}
		}
		wg.Wait()
		grpclog.Printf("retrieveDevices: QuoteChannels: %v \n", quoteChannels)
		r.broadcastQuoteToListeners(quoteID, senderID, senderNinjaName, quoteChannels)
		deviceChannel <- deviceChan{devices, nil}
	}(userChannel)

	return deviceChannel
}

func (r *MongoRepository) pushQuoteViaGCM(userSession *schema.Session, quoteChannel chan quoteChan,
	deviceChannel chan deviceChan) chan gcmChan {
	gcmChannel := make(chan gcmChan)

	go func(userSession *schema.Session, quoteChannel chan quoteChan, deviceChannel chan deviceChan) {
		deviceResp := <-deviceChannel
		defer close(gcmChannel)

		if deviceResp.err != nil {
			gcmChannel <- gcmChan{nil, deviceResp.err}
			return
		} else if len(deviceResp.devices) == 0 {
			gcmChannel <- gcmChan{nil, grpc.Errorf(codes.OK, "No receipients!")}
			return
		}

		// Retrive the Quote to send
		quoteResp := <-quoteChannel
		if quoteResp.err != nil {
			gcmChannel <- gcmChan{nil, quoteResp.err}
			return
		}
		// Send the message!
		grpclog.Printf("[%s] - pushQuoteViaGCM: quote (%s) to (%d devices)\n", userSession.SessionId,
			quoteResp.quote.Quote, len(deviceResp.devices))
		message := quoteToMap(quoteResp.quote, userSession, "quote")
		if message == nil {
			gcmChannel <- gcmChan{nil, grpc.Errorf(codes.Canceled, "Something went wrong with quote")}
			return
		}

		msg := gcm.NewMessage(message, deviceResp.devices...)
		msg.CollapseKey = "NinjaQuote"
		// msg.TimeToLive = 3600 // Time to live in seconds.
		//
		// Create a Sender to send the message.
		sender := &gcm.Sender{ApiKey: r.googleConfig.APIKey}

		// Send the message and receive the response after at most two retries.
		response, err := sender.Send(msg, 2)

		if err != nil {
			gcmChannel <- gcmChan{nil, grpc.Errorf(codes.Internal, "pushQuoteViaGCM: %v", err)}
			return
		}
		gcmChannel <- gcmChan{response, grpc.Errorf(codes.OK, "")}

		// TODO: Break out into itsown go routine?
		var devicesToRemove []bson.M

		for index, result := range response.Results {
			if strings.EqualFold(result.Error, "NotRegistered") {
				devicesToRemove = append(devicesToRemove, bson.M{"token": deviceResp.devices[index]})
			}
		}

		if len(devicesToRemove) > 0 {
			r.RemoveExpiredTokens(devicesToRemove, userSession)
		}
		return
	}(userSession, quoteChannel, deviceChannel)

	return gcmChannel
}

func (r *MongoRepository) insertUser(user *pb.RegisterUser, pwChannel chan pwChan) chan userIDChan {
	userChannel := make(chan userIDChan)
	go func(user *pb.RegisterUser, pwChannel chan pwChan) {
		var (
			newUser schema.UserProfile
			id      = primitive.NewObjectID()
		)
		defer close(userChannel)

		pw := <-pwChannel
		if pw.err != nil {
			userChannel <- userIDChan{nil, "", grpc.Errorf(codes.Internal, "%v", pw.err)}
			return
		}

		newUser = schema.UserProfile{
			ID:        id,
			Name:      user.Name,
			Email:     user.Email,
			LoginType: user.LoginType,
			UserName:  user.Username,
			Password:  string(pw.password),
			NinjaName: uppercaseInitial(ninja.Name(strings.Split(user.Name, " ")[0])),
			Devices:   make([]schema.Device, 0),
			Meta: schema.UserProfileMeta{
				CreationDate: time.Now(),
			},
		}

		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		_, err := r.Client.Database(r.DatabaseName).Collection(r.UserCollection).InsertOne(ctx, &newUser)
		if err != nil {
			userChannel <- userIDChan{nil, "", grpc.Errorf(codes.Internal, "%v", err)}
			return
		}

		userChannel <- userIDChan{&id, newUser.NinjaName, grpc.Errorf(codes.OK, "User Registered Successfully")}
		return
	}(user, pwChannel)
	return userChannel
}

func (r *MongoRepository) cleanupLocaluser(deviceID string) {
	var localUser schema.UserProfile

	userCollection := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)
	sessionCollection := r.Client.Database(r.DatabaseName).Collection(r.SessionCollection)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result := userCollection.FindOne(ctx, bson.M{"name": deviceID})
	result.Decode(&localUser)
	if localUser.Name != "" {
		userCollection.DeleteOne(ctx, bson.M{"_id": localUser.ID})
		sessionCollection.DeleteOne(ctx, bson.M{"user_id": localUser.ID})
	}

}

func (r *MongoRepository) changeSessionUpdate() bson.M {
	return bson.M{"$set": bson.M{"last_activity": time.Now()}}
}

// GetTempQuoteID returns the Object ID generated on start
func (r *MongoRepository) GetTempQuoteID() primitive.ObjectID {
	return r.tempObjectID
}
