package repository

/**
 * Channel type declarations
 * TODO: move to it's own package under repository as it will help keep things a bit clearer and can also split
 * definitions into files with names that relate to the channel's use
 */

import (
	"bitbucket.org/altomapps/jinchi/lib/schema"
	gcm "github.com/therahulprasad/go-fcm"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// quoteChan carries quotes with any errors that occured during retrieval/conversion between go routines
type quoteChan struct {
	quote *schema.Quote // Reference to the quote being transfered.
	err   error         // Error that occured while retrieving/processing the quote we were supposed to transfer
}

// suggestionChan carries suggestions for search typeahead along with any errors that might have occured
// between go routines
type suggestionChan struct {
	suggestion *schema.Suggestion
	err        error
}

// userChan carries a list of user profiles between go routines
// TODO: convert to a proper streaming channel as there is no need to transfer the whole list at once.
type userChan struct {
	users []schema.UserProfile
	err   error
}

// deviceChan carries a list of device token used to send notifications
// TODO: check if it makes sense to convert to a "stream"
type deviceChan struct {
	devices []string // List of FCM tockens
	err     error
}

// gcmChan carries the respnose of FCM betwen go routines
type gcmChan struct {
	response *gcm.Response
	err      error
}

// userIDChan carries a response to a user registration.
type userIDChan struct {
	id        *primitive.ObjectID
	ninjaName string
	err       error
}
