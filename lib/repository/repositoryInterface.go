package repository

import (
	"bitbucket.org/altomapps/jinchi/lib/schema"
	pb "bitbucket.org/altomapps/jinchi/protobuf/endpoints"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Enum containing all the options for supported database systems.
const (
	// MongoDB handles CRUD operations to a mongo database
	MongoDB = 1 << iota
)

const (
	// EnvDev identifier for the development environment
	EnvDev = 1 << iota
	// EnvProd identifier for the production environment
	EnvProd
)

var bcryptCost = 10

// Repository is an interface to a repository pattern.
type Repository interface {
	QuoteGetByID(*pb.QuoteRequest) (*pb.QuoteResponse, error)
	QuoteListCategory(*pb.QuoteCategoryRequest, pb.JinchiService_QuoteCategoryServer, *schema.Session) error
	// QuoteSend needs to be able to do the following actions:
	//           - Select 8 random registered users that:
	//              * Don't include the user sending the quote.
	//              * Have NOT received another quote (today?)
	//              * Update the sent time and Attack count of the sending user
	//              * Update the received time and Received count for the selected users
	QuoteSend(*pb.QuoteSendRequest, *schema.Session) (*pb.QuoteSendResponse, error)
	Arigatou(*pb.ArigatouRequest, *schema.Session) (*pb.ArigatouResponse, error)
	QuoteTypeAhead(*pb.QuoteTypeAheadRequest, pb.JinchiService_TypeAheadServer) error

	// Search
	SearchQuotes(*pb.SearchQuotesRequest, pb.JinchiService_SearchQuotesServer, *schema.Session) error

	RegisterDevice(*pb.RegisterDevice, *schema.Session) (*pb.RegistrationResponse, error)
	RegisterUser(*pb.RegisterUser) (*pb.RegistrationResponse, error)

	Login(*pb.LoginMessage) (*pb.LoginResponse, error)
	Logout(*pb.LogoutMessage) (*pb.LogoutResponse, error)

	UserList(*pb.UserListMessage, *schema.Session) (*pb.UserListResponse, error)
	QuoteToUserList(*pb.QuoteToUserListMessage, *schema.Session) (*pb.QuoteToUserListResponse, error)
	QuoteUserList(*pb.QuoteUserListMessage, pb.JinchiService_QuoteUserListServer, *schema.Session) error
	QuoteUserListExtended(*pb.QuoteUserListMessage, pb.JinchiService_QuoteUserListExtendedServer, *schema.Session) error
	UserProfile(*schema.Session) (*pb.UserProfileResponse, error)

	VerifySession(string) *schema.Session
	GetTempQuoteID() primitive.ObjectID

	// motd

	GetMotd() *schema.Motd
	GetQuoteOfTheDay(*schema.Motd) *schema.Quote
	GetActiveDevices() []schema.UserProfile
	SendMotd(*primitive.ObjectID)
	RemoveExpiredTokens(devicesToRemove []bson.M, userSession *schema.Session)
	Close()
}

// GetRepository returns the selected database object
func GetRepository(db uint32, env string) Repository {
	switch db {
	case MongoDB:
		return NewMongoRepository(env)
	}
	return nil
}
