package repository

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/grpclog"

	"github.com/kardianos/osext"
	gcm "github.com/therahulprasad/go-fcm"
	"github.com/twinj/uuid"
	"golang.org/x/crypto/bcrypt"

	"bitbucket.org/altomapps/jinchi/lib/schema"
	pb "bitbucket.org/altomapps/jinchi/protobuf/endpoints"
)

// GoogleConfig contains google services specific information like APIKeys
type GoogleConfig struct {
	APIKey string `json:"api_key"`
}

// MongoConfig contains the configuration of all the mongo environments
type MongoConfig struct {
	Dev  *MongoRepository `json:"dev"`
	Prod *MongoRepository `json:"prod"`
}

// MongoRepository is the interface that manages the data retrieval from MongoDB.
type MongoRepository struct {
	ConnectionString         string        `json:"connection_string"`
	DatabaseName             string        `json:"database_name"`
	Client                   *mongo.Client `json: "client,omitempty"`
	UserCollection           string        `json:"user_collection"`
	SessionCollection        string        `json:"session_collection"`
	QuoteCollection          string        `json:"quote_collection"`
	CategoryCollection       string        `json:"category_collection"`
	WordCollection           string        `json:"word_collection"`
	MotdCollection           string        `json:"motd_collection"`
	UserCollectionIndexes    []Index       `json:"user_collection_indexes,omitempty"`
	SessionCollectionIndexes []Index       `json:"session_collection_indexes,omitempty"`
	QuoteCollectionIndexes   []Index       `json:"quote_collection_indexes,omitempty"`
	WordCollectionIndexes    []Index       `json:"word_collection_indexes,omitempty"`
	MotdCollectionIndexes    []Index       `json:"motd_collection_indexes,omitempty"`
	tempObjectID             primitive.ObjectID
	environment              uint32
	googleConfig             *GoogleConfig
}

// NewMongoRepository initializes a new MongoRepository structure and returns it.
func NewMongoRepository(env string) *MongoRepository {
	fmt.Println(env)
	mongoRepo := new(MongoRepository)
	execDir, err := osext.ExecutableFolder()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	mongoConfig := loadMongoConfig(execDir + "/config/mongo.json")

	switch env {
	case "dev":
		mongoRepo = mongoConfig.Dev
		mongoRepo.environment = EnvDev
	case "prod":
		mongoRepo = mongoConfig.Prod
		mongoRepo.environment = EnvProd
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	mongoRepo.Client, err = mongo.Connect(ctx, options.Client().ApplyURI(mongoRepo.ConnectionString))

	if err != nil {
		fmt.Printf("Couldn't connect to Mongo: %v\n", err)
		grpclog.Fatalf("Couldn't connect to Mongo: %v", err)
		return nil
	}

	err = ensureIndex(mongoRepo.UserCollection, mongoRepo.DatabaseName, mongoRepo.UserCollectionIndexes, mongoRepo.Client)
	if err != nil {
		grpclog.Fatalf("Error creating user indexes %v", err.Error())
	}
	err = ensureIndex(mongoRepo.SessionCollection, mongoRepo.DatabaseName, mongoRepo.SessionCollectionIndexes, mongoRepo.Client)
	if err != nil {
		grpclog.Fatalf("Error creating session indexes %v", err.Error())
	}
	err = ensureIndex(mongoRepo.QuoteCollection, mongoRepo.DatabaseName, mongoRepo.QuoteCollectionIndexes, mongoRepo.Client)
	if err != nil {
		grpclog.Fatalf("Error creating user indexes %v", err.Error())
	}
	mongoRepo.tempObjectID = primitive.NewObjectID()

	mongoRepo.googleConfig = loadGoogleConfig(execDir + "/config/google.json")
	fmt.Println("New Repository done!")
	return mongoRepo
}

// QuoteGetByID retrieves the quote with the id contained in the QuoteRequest message from the database and returns
// a QuoteResponse with its contents.
func (r *MongoRepository) QuoteGetByID(request *pb.QuoteRequest) (*pb.QuoteResponse, error) {
	var quote schema.Quote

	// session := r.Session.Copy()
	// defer session.Close()

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	quotesCollection := r.Client.Database(r.DatabaseName).Collection(r.QuoteCollection)

	if bytes.Compare(request.QuoteId, []byte("************")) != 0 {
		quoteID, _ := primitive.ObjectIDFromHex(hex.EncodeToString(request.QuoteId))

		err := quotesCollection.FindOne(ctx, bson.M{"_id": quoteID}).Decode(&quote)
		if err != nil {
			return new(pb.QuoteResponse), grpc.Errorf(codes.NotFound, err.Error())
		}
	} else {
		rand.Seed(time.Now().Unix())
		err := quotesCollection.FindOne(ctx, bson.M{"random_point": bson.M{
			"$near": []float64{rand.Float64(), rand.Float64()},
		}}).Decode(&quote)
		if err != nil {
			return new(pb.QuoteResponse), grpc.Errorf(codes.Internal, err.Error())
		}
	}

	return quoteToPB(&quote)
}

// QuoteListCategory retrieves all the quotes of a category.
func (r *MongoRepository) QuoteListCategory(request *pb.QuoteCategoryRequest,
	stream pb.JinchiService_QuoteCategoryServer, userSession *schema.Session) error {
	quotes := make(chan quoteChan)
	go r.retrieveCategoryQuotes(request, quotes, userSession)

	// Convert to response list
	for schemaQuote := range quotes {
		if schemaQuote.err != nil {
			return schemaQuote.err
		}
		pbQuote, err := quoteToPB(schemaQuote.quote)
		if err != nil {
			return grpc.Errorf(codes.Unknown, err.Error())
		}
		if err := stream.Send(pbQuote); err != nil {
			return err
		}
	}

	return grpc.Errorf(codes.OK, "")
}

// QuoteSend needs to be able to do the following actions:
//           - Select 8 random registered users that:
//              * Don't include the user sending the quote.
//              * Have NOT received another quote (today?)
//              * Update the sent time and Attack count of the sending user
//              * Update the received time and Received count for the selected users
func (r *MongoRepository) QuoteSend(request *pb.QuoteSendRequest, userSession *schema.Session) (*pb.QuoteSendResponse,
	error) {
	var (
		quoteID primitive.ObjectID

		// session = r.Session.Copy()
	)
	// defer session.Close()

	if quoteID = byteArrayToObjectID(request.QuoteId, r); quoteID == r.GetTempQuoteID() {
		return new(pb.QuoteSendResponse), grpc.Errorf(codes.FailedPrecondition, "Quote ID not supplied")
	}

	// Send push notification
	gcmResp := <-r.pushQuoteViaGCM(userSession,
		r.retrieveQuote(quoteID),
		r.retrieveDevices(r.retrieveSendUsers(userSession), quoteID, userSession.UserId, userSession.NinjaName))

	if gcmResp.err != nil {
		return new(pb.QuoteSendResponse), grpc.Errorf(codes.DataLoss, "Failed to send message: %v", gcmResp.err)
	}
	prettyResponse, _ := json.Marshal(gcmResp.response)
	grpclog.Printf("[%s] - QuoteSent: %s\n", userSession.SessionId, string(prettyResponse))

	// Update sender stats.
	if err := r.updateSender(quoteID, userSession); err != nil {
		return new(pb.QuoteSendResponse), err
	}

	return &pb.QuoteSendResponse{Status: 0}, nil
}

// Arigatou sends the arigaatou tou the sender, while updating the relative counters and lists
func (r *MongoRepository) Arigatou(in *pb.ArigatouRequest, userSession *schema.Session) (*pb.ArigatouResponse, error) {
	senderID := byteArrayToObjectID(in.Sender, r)
	devices, err := r.getUserDevices(senderID)
	quoteID := byteArrayToObjectID(in.QuoteId, r)

	if err != nil {
		fmt.Printf("Arigatou - getUserDevices: %v\n", err)
		return new(pb.ArigatouResponse), err
	} else if len(devices) == 0 {
		return new(pb.ArigatouResponse), grpc.Errorf(codes.Canceled, "Couldn't find Sender's(%v) device(s)", senderID)
	}

	message := ninjaNameToMap(userSession)
	if message == nil {
		return new(pb.ArigatouResponse), grpc.Errorf(codes.Canceled, "Something went wrong with quote")
	}

	msg := gcm.NewMessage(message, devices...)
	// TODO: we already have 2 of these we need to start thinking about formalizing them somehow.
	msg.CollapseKey = "arigatou"

	sender := &gcm.Sender{ApiKey: r.googleConfig.APIKey}

	// Send the message and receive the response after at most two retries.
	response, err := sender.Send(msg, 2)
	if err != nil {
		fmt.Printf("Arigatou - GCM: %v\n", err)
		return new(pb.ArigatouResponse),
			grpc.Errorf(codes.Canceled, "Something went wrong while trying to send through GCM: %s", err.Error())
	}

	prettyResponse, _ := json.Marshal(response)
	grpclog.Printf("[%s] - Arigatou: %s\n", userSession.SessionId, string(prettyResponse))

	// Update sender and receiver counters.
	r.increaseUserCounter([]string{"arigatou.sent"}, userSession.UserId, senderID)
	if response.Success > 0 {
		r.increaseUserCounter([]string{"arigatou.received"}, senderID, userSession.UserId)
	}

	go r.updateArigatouDate(quoteID, senderID, userSession)

	id, err := hex.DecodeString(quoteID.Hex())
	if err != nil {
		fmt.Printf("Arigatou - Response: %v\n", err)
		return nil, grpc.Errorf(codes.Internal, "Something went wrong while trying to send the response: %s",
			err.Error())
	}

	return &pb.ArigatouResponse{Status: 1, QuoteId: id}, nil
}

// QuoteTypeAhead returns suggestions based on what user has typed in the searchbox
func (r *MongoRepository) QuoteTypeAhead(request *pb.QuoteTypeAheadRequest,
	stream pb.JinchiService_TypeAheadServer) error {
	suggestions := make(chan suggestionChan)
	go r.typeAheadSuggestions(request, suggestions)

	// Convert to response list
	for schemaSuggestion := range suggestions {
		if schemaSuggestion.err != nil {
			return schemaSuggestion.err
		}
		if err := stream.Send(&pb.QuoteTypeAheadResponse{Suggestion: schemaSuggestion.suggestion.Id}); err != nil {
			return err
		}
		grpclog.Printf("QuoteTypeAhead - Suggestion: %s", schemaSuggestion.suggestion.Id)
	}

	return grpc.Errorf(codes.OK, "")
}

// SearchQuotes returns the matching quotes based on the search terms.
func (r *MongoRepository) SearchQuotes(request *pb.SearchQuotesRequest, stream pb.JinchiService_SearchQuotesServer,
	userSession *schema.Session) error {
	quotes := make(chan quoteChan)

	go r.retrieveSearchQuotes(request, quotes, userSession)

	// Convert to response list
	for schemaQuote := range quotes {
		if schemaQuote.err != nil {
			return schemaQuote.err
		}
		pbQuote, err := quoteToPB(schemaQuote.quote)
		if err != nil {
			return grpc.Errorf(codes.Unknown, err.Error())
		}
		if err := stream.Send(pbQuote); err != nil {
			return err
		}
	}

	return grpc.Errorf(codes.OK, "")
}

// RegisterDevice registers a device for push notifications
func (r *MongoRepository) RegisterDevice(device *pb.RegisterDevice, userSession *schema.Session) (
	*pb.RegistrationResponse, error) {

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	userCollection := r.Client.Database(r.DatabaseName).Collection(r.UserCollection)

	// Update token if the device ID exists.
	opts := options.Update().SetUpsert(true)
	changeInfo, err := userCollection.UpdateMany(ctx, bson.M{"devices.device_id": device.DeviceId},
		bson.M{
			"$max": bson.M{"app_version": device.Version},
			"$set": bson.M{"devices.$.token": device.Token},
		}, opts)
	if (changeInfo.UpsertedCount == 0) || (err != nil) {

		changeInfo, err = userCollection.UpdateMany(ctx,
			bson.M{"$and": []bson.M{
				bson.M{"_id": userSession.UserId},
				bson.M{"devices.token": device.Token}},
			},
			bson.M{
				"$max": bson.M{"app_version": device.Version},
				"$set": bson.M{"devices.$.device_id": device.DeviceId},
			}, opts)
		if (changeInfo.MatchedCount == 0) || (err != nil) {
			fmt.Printf("[%s] - RegisterDevice FAILED  [%v]\n (%v)\n", userSession.SessionId, changeInfo, err)
			deviceUpdate := bson.M{
				"$max": bson.M{"app_version": device.Version},
				"$addToSet": bson.M{"devices": bson.M{
					"device_id": device.DeviceId,
					"token":     device.Token,
				}},
			}
			_, err = userCollection.UpdateOne(ctx,
				bson.M{"_id": userSession.UserId, "devices.token": bson.M{"$elemMatch": bson.M{"$ne": device.Token}}},
				deviceUpdate, opts)
		}
		if err != nil {
			fmt.Printf("[%s] - RegisterDevice FAILED  [%v]\n (%v)\n", userSession.SessionId, changeInfo, err)
			return new(pb.RegistrationResponse), err
		}
	} else if err != nil {
		fmt.Printf("[%s] - RegisterDevice FAILED  [%v]\n (%v)\n", userSession.SessionId, changeInfo, err)
		return new(pb.RegistrationResponse), err
	}

	prettyDevice, _ := json.Marshal(device)
	fmt.Printf("[%s] - RegisterDevice: %s\n", userSession.SessionId, string(prettyDevice))

	go r.cleanupLocaluser(device.DeviceId)

	idBytes, err := userSession.UserId.MarshalJSON()
	if err != nil {
		return new(pb.RegistrationResponse), err
	}
	return &pb.RegistrationResponse{Id: idBytes}, nil
}

// RegisterUser adds new users to the database
func (r *MongoRepository) RegisterUser(user *pb.RegisterUser) (*pb.RegistrationResponse, error) {
	newUser := <-r.insertUser(user, encryptPassword(user.Password))
	if newUser.err != nil {
		return new(pb.RegistrationResponse), newUser.err
	}

	idBytes, err := newUser.id.MarshalJSON()
	if err != nil {
		return new(pb.RegistrationResponse), grpc.Errorf(codes.Internal, "%v", err)
	}

	// if (len(user.DeviceId) > 0) && (user.LoginType != pb.LoginType_LOCAL) {
	// 	err = r.cleanupTempUsers(user.DeviceId, user.Email)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// }

	return &pb.RegistrationResponse{Id: idBytes, NinjaName: newUser.ninjaName}, nil
}

// Login a user using the provided credentials
func (r *MongoRepository) Login(login *pb.LoginMessage) (*pb.LoginResponse, error) {
	var (
		user          schema.UserProfile
		userSession   schema.Session
		loginResponse pb.LoginResponse
	)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := r.Client.Database(r.DatabaseName).Collection(r.UserCollection).FindOne(ctx, bson.M{"$and": []bson.M{
		bson.M{"user_name": login.Username},
		bson.M{"login_type": login.LoginType},
	}}).Decode(&user)
	if err != nil {
		fmt.Printf("Login user ERROR (%v)  \n", err)
		return new(pb.LoginResponse), grpc.Errorf(codes.FailedPrecondition, "1 - %s", err.Error())
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(login.Password))
	if err != nil {
		fmt.Printf("Login user ERROR (%v)  \n", err)
		return new(pb.LoginResponse), grpc.Errorf(codes.FailedPrecondition, "2 - %s", err.Error())
	}

	coll := r.Client.Database(r.DatabaseName).Collection(r.SessionCollection)
	filter := bson.M{"user_id": user.ID}
	count, err := coll.CountDocuments(ctx, filter)

	if err != nil { // Something went wrong
		fmt.Printf("Login user ERROR (%v)  \n", err)
		return new(pb.LoginResponse), grpc.Errorf(codes.Internal, "%s", err.Error())
	} else if count == 0 { // New session
		userSession.SessionId = uuid.NewV4().String()
		userSession.UserId = user.ID
		if user.LoginType == pb.LoginType_LOCAL {
			userSession.SessionType = "temp"
		} else {
			userSession.SessionType = "full"
		}
		userSession.LastActivity = time.Now()
		userSession.NinjaName = user.NinjaName
		userSession.Lists.Favourites = append(userSession.Lists.Favourites, user.Lists.Favourites...)
		userSession.Lists.Hidden = append(userSession.Lists.Hidden, user.Lists.Hidden...)
		userSession.Lists.Sent = append(userSession.Lists.Sent, user.Lists.Sent...)
		userSession.Lists.Received = append(userSession.Lists.Received, user.Lists.Received...)

		_, err = r.Client.Database(r.DatabaseName).Collection(r.SessionCollection).InsertOne(ctx, &userSession)
		if err != nil {
			return new(pb.LoginResponse), grpc.Errorf(codes.FailedPrecondition, "3 - %s", err.Error())
		}
	} else { // Existing Session
		opts := options.FindOneAndUpdate().SetReturnDocument(options.After)
		result := coll.FindOneAndUpdate(ctx, filter, r.changeSessionUpdate(), opts)
		if result.Err() != nil {
			return new(pb.LoginResponse), grpc.Errorf(codes.FailedPrecondition, "4 - %s", err.Error())
		}
		result.Decode(&userSession)
	}

	loginResponse.SessionId = userSession.SessionId
	loginResponse.NinjaName = user.NinjaName
	loginResponse.Favourites = objectIDsToByteArrays(userSession.Lists.Favourites)

	return &loginResponse, nil
}

// Logout destroys an active user session
func (r *MongoRepository) Logout(logout *pb.LogoutMessage) (*pb.LogoutResponse, error) {
	var logoutResponse pb.LogoutResponse

	// session := r.Session.Copy()
	// defer session.Close()

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	_, err := r.Client.Database(r.DatabaseName).Collection(r.SessionCollection).DeleteOne(ctx, bson.M{"session_id": logout.SessionId})
	if err != nil {
		// Something has gone wrong with logging out so let the client know
		logoutResponse.Status = pb.LogoutStatus_ERROR
	} else {
		logoutResponse.Status = pb.LogoutStatus_OK
	}

	return &logoutResponse, err
}

// UserList returns the contents of the requested user list
func (r *MongoRepository) UserList(list *pb.UserListMessage, userSession *schema.Session) (*pb.UserListResponse,
	error) {
	switch list.List {
	case pb.ListType_FAVOURITES:
		return &pb.UserListResponse{QuoteId: objectIDsToByteArrays(userSession.Lists.Favourites)}, nil
	case pb.ListType_SENT:
		return &pb.UserListResponse{QuoteId: objectIDsToByteArrays(userSession.Lists.Sent)}, nil
	case pb.ListType_HIDDEN:
		return &pb.UserListResponse{QuoteId: objectIDsToByteArrays(userSession.Lists.Hidden)}, nil
	}

	return new(pb.UserListResponse), grpc.Errorf(codes.NotFound, "User list not found!")
}

// QuoteToUserList adds or removes a quote to a user list.
func (r *MongoRepository) QuoteToUserList(quoteAction *pb.QuoteToUserListMessage, userSession *schema.Session) (
	*pb.QuoteToUserListResponse, error) {
	var (
		quoteToUserListResponse pb.QuoteToUserListResponse
		listName                string
		listAction              string
		quoteID                 primitive.ObjectID
	)
	// session := r.Session.Copy()
	// defer session.Close()

	listName = strings.ToLower(concatinateStrings("lists.", pb.ListType_name[int32(quoteAction.Where)]))

	switch quoteAction.What {
	case pb.ListAction_ADD:
		listAction = "$addToSet"
	case pb.ListAction_REMOVE:
		listAction = "$pull"
	}

	if quoteID = byteArrayToObjectID(quoteAction.QuoteId, r); quoteID == r.GetTempQuoteID() {
		return new(pb.QuoteToUserListResponse), grpc.Errorf(codes.FailedPrecondition, "Quote ID not supplied")
	}

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	opts := options.Update().SetUpsert(true)
	_, err := r.Client.Database(r.DatabaseName).Collection(r.UserCollection).
		UpdateOne(ctx, bson.M{"_id": userSession.UserId}, bson.M{listAction: bson.M{listName: quoteID}}, opts)
	if err != nil {
		return new(pb.QuoteToUserListResponse), grpc.Errorf(codes.Internal, "Could not update user record: %v", err)
	}

	_, err = r.Client.Database(r.DatabaseName).Collection(r.SessionCollection).
		UpdateOne(ctx, bson.M{"_id": userSession.ID}, bson.M{listAction: bson.M{listName: quoteID}}, opts)
	if err != nil {
		return new(pb.QuoteToUserListResponse),
			grpc.Errorf(codes.Internal, "Could not update user session record %v", err)
	}

	quoteToUserListResponse.Count = 1
	return &quoteToUserListResponse, nil
}

// QuoteUserList returns a stream of quotes the user has added in the requested list.
func (r *MongoRepository) QuoteUserList(in *pb.QuoteUserListMessage, stream pb.JinchiService_QuoteUserListServer,
	userSession *schema.Session) error {
	var (
		list *[]primitive.ObjectID
	)

	switch in.List {
	case pb.ListType_FAVOURITES:
		list = &userSession.Lists.Favourites
	case pb.ListType_HIDDEN:
		list = &userSession.Lists.Hidden
	case pb.ListType_SENT:
		list = &userSession.Lists.Sent
	}

	quotes := make(chan quoteChan)

	go r.retrieveListQuotes(list, in.LastQuoteId, in.Limit, quotes)

	// Convert to response list
	for quote := range quotes {
		if quote.err != nil {
			return quote.err
		}
		pbQuote, err := quoteToPB(quote.quote)
		if err != nil {
			return grpc.Errorf(codes.Unknown, err.Error())
		}
		if err := stream.Send(pbQuote); err != nil {
			return err
		}
	}

	return grpc.Errorf(codes.OK, "")
}

// QuoteUserListExtended streams a quote list that includes information like date, sender and arigatou
func (r *MongoRepository) QuoteUserListExtended(in *pb.QuoteUserListMessage,
	stream pb.JinchiService_QuoteUserListExtendedServer, userSession *schema.Session) error {
	var (
		errors error
		list   *[]schema.ReceivedList
	)

	switch in.List {
	case pb.ListType_RECEIVED:
		list = &userSession.Lists.Received
	}
	// TODO: For now such lists need to be transfered in whole, look into how you can continue from where it finished.
	// i := sort.Search(len(list), func(i int) bool {
	// 	return list[i].Quote.Id == primitive.ObjectIDHex(hex.EncodeToString(in.LastQuoteId))
	// })

	if len(*list) > 0 {
		// If we have received quotes before send these back.
		err := receivedListToPBStream(list, stream)
		if err != nil {
			errors = grpc.Errorf(codes.Internal, "%v; %v", errors, err)
		}
	} else {
		// otherwise send the default one.
		response := new(pb.QuoteUserListExtendedResponse)
		response.Quote = &pb.QuoteResponse{
			Id:        []byte{0x57, 0x0c, 0x27, 0x13, 0xd5, 0xfe, 0xe5, 0x9f, 0x52, 0x8b, 0x2a, 0xc5},
			Category:  []byte{0x56, 0xd7, 0x76, 0xd5, 0x35, 0x3c, 0x11, 0x70, 0x04, 0x03, 0x82, 0xf5},
			Quote:     "People often say that motivation doesn't last. Well... neither does bathing that's why we recommend it daily",
			Reference: "Zig Ziglar",
			ImgUrl:    "",
			Sender: "	Lumotochikaari",
			Senderid: []byte{},
		}
		response.Date = time.Now().Unix()
		response.Arigatou = time.Time{}.Unix()

		err := stream.Send(response)
		if err != nil {
			errors = grpc.Errorf(codes.Internal, "%v; %v", errors, err)
		}

		quoteChannel := make(chan *schema.ReceivedList)
		objectID, _ := primitive.ObjectIDFromHex("570c2713d5fee59f528b2ac5")
		received := &schema.ReceivedList{
			Quote: schema.Quote{
				Id:        objectID,
				Reference: response.GetQuote().GetReference(),
				Quote:     response.GetQuote().GetQuote(),
			},
			SenderID:   *(new(primitive.ObjectID)),
			SenderName: response.GetQuote().GetSender(),
			Date:       time.Now(),
			Arigatou:   time.Time{},
		}

		go r.addQuoteToUserList(pb.ListType_RECEIVED, userSession.UserId, quoteChannel)
		quoteChannel <- received
	}
	return errors
}

// UserProfile retrieves profile information for the current user
func (r *MongoRepository) UserProfile(userSession *schema.Session) (*pb.UserProfileResponse, error) {
	response := &pb.UserProfileResponse{
		Quotes:   &pb.ProfileCounts{},
		Arigatou: &pb.ProfileCounts{},
	}
	user := new(schema.UserProfile)
	// session := r.Session.Copy()
	// defer session.Close()

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := r.Client.Database(r.DatabaseName).Collection(r.UserCollection).FindOne(ctx, bson.M{"_id": userSession.UserId}).Decode(user)
	grpclog.Printf("%v\n", user)
	grpclog.Printf("%v\n", response)

	response.NinjaName = user.NinjaName
	response.Quotes.Received = user.Meta.Received
	response.Quotes.Sent = user.Meta.Attack
	response.Arigatou.Received = user.Meta.Arigatou.ReceivedCounter
	response.Arigatou.Sent = user.Meta.Arigatou.SentCounter

	if err != nil {
		return new(pb.UserProfileResponse), grpc.Errorf(codes.Internal, "Could not retrieve user profile %v ", err)
	}

	return response, nil
}

// VerifySession verifies that the provided session exists and is still valid.
func (r *MongoRepository) VerifySession(sessionID string) *schema.Session {
	userSession := new(schema.Session)

	// session := r.Session.Copy()
	// defer session.Close()
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result := r.Client.Database(r.DatabaseName).Collection(r.SessionCollection).FindOneAndUpdate(ctx,
		bson.M{"$and": []bson.M{
			bson.M{"session_id": sessionID},                                         // Verify the session_id exists,
			bson.M{"last_activity": bson.M{"$gte": time.Now().Add(-1 * time.Hour)}}, // that it is still valid
		}}, r.changeSessionUpdate()) // and update last activity time

	if result.Err() != nil {
		grpclog.Printf("Couldn't verify session: %v\n", result.Err())
		return nil
	}

	result.Decode(&userSession)

	return userSession
}

func loadMongoConfig(configFile string) *MongoConfig {
	var mongoConfig MongoConfig

	file, e := ioutil.ReadFile(configFile)
	if e != nil {
		log.Fatalf("File error: %v\n", e)
	}

	mongoConfig.Dev = new(MongoRepository)
	mongoConfig.Prod = new(MongoRepository)
	err := json.Unmarshal(file, &mongoConfig)
	if err != nil {
		grpclog.Printf("loadMongoConfig: %v\n", err)
	}

	return &mongoConfig
}

func loadGoogleConfig(configFile string) *GoogleConfig {
	var googleConfig GoogleConfig

	file, e := ioutil.ReadFile(configFile)
	if e != nil {
		log.Fatalf("File error: %v\n", e)
	}

	err := json.Unmarshal(file, &googleConfig)
	if err != nil {
		fmt.Println(err)
	}

	return &googleConfig
}

// ensureIndex wraps the check/creation of indexes for collections
func ensureIndex(collection, database string, indexes []Index, client *mongo.Client) error {
	for _, index := range indexes {
		col := client.Database(database).Collection(collection)
		indexView := col.Indexes()
		keysDoc := bsonx.Doc{}

		for _, key := range index.Key {
			keysDoc = keysDoc.Append(key, bsonx.Int32(1))
		}

		opts := options.Index().SetUnique(index.Options.Unique).SetBackground(index.Options.Background)
		if index.Options.ExpireAfterSeconds != 0 {
			opts = opts.SetExpireAfterSeconds(index.Options.ExpireAfterSeconds)
		}
		idx := mongo.IndexModel{
			Keys:    keysDoc,
			Options: opts,
		}
		_, err := indexView.CreateOne(context.Background(), idx)
		if err != nil {
			return err
		}
	}

	return nil
}
