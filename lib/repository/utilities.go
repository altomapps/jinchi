package repository

import (
	"bytes"
	"encoding/hex"
	"unicode"

	"google.golang.org/grpc/grpclog"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"bitbucket.org/altomapps/jinchi/lib/schema"
	pb "bitbucket.org/altomapps/jinchi/protobuf/endpoints"
	"golang.org/x/crypto/bcrypt"
)

type pwChan struct {
	password []byte
	err      error
}

func adjustLimit(offset, limit, len uint32) uint32 {
	newLimit := limit

	if (offset + limit) > len {
		newLimit = len - offset
	} else if limit == 0 {
		newLimit = len
	}

	return newLimit
}

func quoteToPB(quote *schema.Quote) (*pb.QuoteResponse, error) {
	var response pb.QuoteResponse

	id, err := hex.DecodeString(quote.Id.Hex())
	if err != nil {
		return new(pb.QuoteResponse), err
	}

	category, err := hex.DecodeString(quote.Category.Hex())
	if err != nil {
		return new(pb.QuoteResponse), err
	}

	response.Id = id
	response.Category = category
	response.Quote = quote.Quote
	response.Reference = quote.Reference
	response.ImgUrl = quote.Img

	return &response, nil
}

// receivedListToPBStream converts the mongoDB record to the equivalent protocol buffers message reporting all the errors
// that might have occured via the returned channel.
// TODO: maybe changed the channel to carry a struct containing quoteID and error so that it can make the debug easier.
func receivedListToPBStream(receivedList *[]schema.ReceivedList, stream pb.JinchiService_QuoteUserListExtendedServer) error {
	grpclog.Printf("Received list length: %d\n", len(*receivedList))
	// loop through the received list and convert it to the correct type for response.
	for _, item := range *receivedList {
		response := new(pb.QuoteUserListExtendedResponse)

		quote, err := quoteToPB(&item.Quote)
		if err != nil {
			return err
		}
		senderID, err := hex.DecodeString(item.SenderID.Hex())
		if err != nil {
			return err
		}

		quote.Sender = item.SenderName
		quote.Senderid = senderID
		response.Quote = quote
		response.Date = item.Date.Unix()
		response.Arigatou = item.Arigatou.Unix()

		err = stream.Send(response)
		if err != nil {
			return err
		}
	}

	return nil
}

// quoteToMap uses quoteToMapBasic to construct the main map and then appends to it the extra fields used by quote
// notifications sent by users
func quoteToMap(quote *schema.Quote, sender *schema.Session, quoteType string) map[string]interface{} {
	quoteMap := quoteToMapBasic(quote, quote.Img, quoteType)
	quoteMap["sender"] = sender.NinjaName
	quoteMap["sender_id"] = sender.UserId.Hex()

	return quoteMap
}

// quoteToMapBasic creates a map with the minimum information required by the Quote Of The DAY, based on
// the quote information and an image url.
func quoteToMapBasic(quote *schema.Quote, quoteImg string, quoteType string) map[string]interface{} {
	return map[string]interface{}{
		"type":      quoteType,
		"msg_type":  quoteType,
		"quote_id":  quote.Id.Hex(),
		"quote":     quote.Quote,
		"reference": quote.Reference,
		"img":       quoteImg,
	}
}

func ninjaNameToMap(sender *schema.Session) map[string]interface{} {
	return map[string]interface{}{
		"msg_type": "arigatou",
		"sender":   sender.NinjaName,
	}
}

func encryptPassword(pw string) chan pwChan {
	pwChannel := make(chan pwChan)

	go func(pw string) {
		password, err := bcrypt.GenerateFromPassword([]byte(pw), bcryptCost)
		defer close(pwChannel)
		if pwChannel != nil {
			pwChannel <- pwChan{password, err}
		}
		return
	}(pw)

	return pwChannel
}

func uppercaseInitial(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

func concatinateStrings(str ...string) string {
	var endString bytes.Buffer

	if str == nil {
		return ""
	}

	for _, word := range str {
		for _, letter := range word {
			endString.WriteRune(letter)
		}
	}

	return endString.String()
}

func byteArrayToObjectID(ID []byte, repository Repository) primitive.ObjectID {
	quoteID := repository.GetTempQuoteID()
	if hexQuoteID := hex.EncodeToString(ID); hexQuoteID != "" {
		quoteID, _ = primitive.ObjectIDFromHex(hexQuoteID)
	}
	return quoteID
}

func objectIDsToByteArrays(list []primitive.ObjectID) [][]byte {
	var quoteIDArray [][]byte

	for _, quoteID := range list {
		id, err := hex.DecodeString(quoteID.Hex())
		if err == nil {
			quoteIDArray = append(quoteIDArray, id)
		}
	}
	return quoteIDArray
}
