package repository

type Options struct {
	Unique             bool  `json: "unique"`
	Background         bool  `json: "background"`
	ExpireAfterSeconds int32 `json: "ExpireAfterSeconds"`
}
type Index struct {
	Key     []string `json: "key"`
	Options Options  `json: "options"`
}
