package rpc

import (
	"fmt"
	"net"

	pb "bitbucket.org/altomapps/jinchi/protobuf/endpoints"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

var grpcServer *grpc.Server

// InitGRPCServer initialises and starts the gRPC server with all the endpoints.
func InitGRPCServer(port int, db uint32, tls bool, env string) {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	grpclog.Printf("Starting GRPC server...\n")

	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}
	//var opts []grpc.ServerOption
	jinchiServer := newServer(db, tls, env)
	opts := jinchiServer.serverOptions()
	grpcServer = grpc.NewServer(*opts...)

	pb.RegisterJinchiServiceServer(grpcServer, jinchiServer)

	go grpcServer.Serve(lis)
	grpclog.Printf("Listening on port: %d", port)
}

// StopGRPCServer does what it says on the tin
func StopGRPCServer() {
	if grpcServer != nil {
		grpclog.Println("Stopping GRPC server")
		grpcServer.Stop()
	}
}
