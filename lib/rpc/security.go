package rpc

import (
	"bitbucket.org/altomapps/jinchi/lib/schema"
	"golang.org/x/net/context"
	"google.golang.org/grpc/metadata"
)

// verifySession wrapper function that extracts the session ID from the context.
func (s *jinchiGRPCServer) verifySession(ctx context.Context) *schema.Session {
	md, ok := metadata.FromIncomingContext(ctx)
	if ok == true {
		if sessionID := md["session"]; sessionID != nil {
			return s.db.VerifySession(md["session"][0])
		}
	}
	return nil
}
