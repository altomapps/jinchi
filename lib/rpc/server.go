package rpc

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"

	"github.com/asaskevich/govalidator"

	"bitbucket.org/altomapps/jinchi/lib/repository"
	pb "bitbucket.org/altomapps/jinchi/protobuf/endpoints"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/grpclog"
)

type jinchiGRPCServer struct {
	db      repository.Repository
	options struct {
		tls                  credentials.TransportCredentials
		maxConcurrentStreams uint32
		compressor           grpc.Compressor
		decompressor         grpc.Decompressor
	}
}

func (s *jinchiGRPCServer) Login(ctx context.Context, in *pb.LoginMessage) (*pb.LoginResponse, error) {
	response, err := s.db.Login(in)

	if err == nil {
		fmt.Printf("[%s] - Login user (%s)  \n", response.SessionId, in.Username)
	} else {
		fmt.Printf("Failed to login user (%s) -  %v \n", in.Username, err)
	}

	return response, err
}

func (s *jinchiGRPCServer) Logout(ctx context.Context, in *pb.LogoutMessage) (*pb.LogoutResponse, error) {
	userSession := s.verifySession(ctx)
	if userSession != nil {
		response, err := s.db.Logout(in)
		if err == nil {
			grpclog.Printf("[%s] - Logout DONE\n", in.SessionId)
		} else {
			grpclog.Printf("[%s] - Logout FAILED (%v)\n", in.SessionId, err)
		}
		return response, err
	}
	return nil, grpc.Errorf(codes.InvalidArgument, "No valid session")
}

func (s *jinchiGRPCServer) DeviceRegistration(ctx context.Context, in *pb.RegisterDevice) (*pb.RegistrationResponse, error) {
	userSession := s.verifySession(ctx)

	if userSession != nil {
		prettyRequest, _ := json.Marshal(in)
		fmt.Printf("[%s] - DeviceRegistration: %s\n", userSession.SessionId, string(prettyRequest))
		response, err := s.db.RegisterDevice(in, userSession)
		if err == nil {
			fmt.Printf("[%s] - DeviceRegistration DONE (%s)\n", userSession.SessionId, hex.EncodeToString(response.Id))
		} else {
			fmt.Printf("[%s] - DeviceRegistration FAILED (%v)\n", userSession.SessionId, err)
		}
		return response, err
	}
	return nil, grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func (s *jinchiGRPCServer) UserRegistration(ctx context.Context, in *pb.RegisterUser) (*pb.RegistrationResponse, error) {
	if govalidator.IsASCII(in.Name) && govalidator.IsEmail(in.Email) {
		fmt.Printf("UserRegistration: (Email:%s, DeviceID:%s, Username:%s, Name:%s, LoginType: %s)", in.Email,
			in.DeviceId, in.Username, in.Name, pb.LoginType_name[int32(in.LoginType)])

		return s.db.RegisterUser(in)
	}
	return nil, grpc.Errorf(codes.InvalidArgument, "Invalid user registration")
}

func (s *jinchiGRPCServer) Quote(ctx context.Context, in *pb.QuoteRequest) (*pb.QuoteResponse, error) {
	userSession := s.verifySession(ctx)
	if userSession != nil && govalidator.IsMongoID(hex.EncodeToString(in.QuoteId)) {
		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - Quote: %s\n", userSession.SessionId, string(prettyRequest))
		response, err := s.db.QuoteGetByID(in)
		if err == nil {
			grpclog.Printf("[%s] - Quote DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - Quote FAILED (%v)\n", userSession.SessionId, err)
		}
		return response, err

	}
	return nil, grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func (s *jinchiGRPCServer) QuoteCategory(in *pb.QuoteCategoryRequest, stream pb.JinchiService_QuoteCategoryServer) error {
	userSession := s.verifySession(stream.Context())
	if userSession != nil && govalidator.IsMongoID(hex.EncodeToString(in.Category)) {
		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - QuoteCategory: %s\n", userSession.SessionId, string(prettyRequest))

		err := s.db.QuoteListCategory(in, stream, userSession)

		if err == nil {
			grpclog.Printf("[%s] -QuoteCategory DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - QuoteCategory FAILED (%v)\n", userSession.SessionId, err)
		}

		return err
	}

	return grpc.Errorf(codes.PermissionDenied, "Not Logged in")
}

func (s *jinchiGRPCServer) QuoteSend(ctx context.Context, in *pb.QuoteSendRequest) (*pb.QuoteSendResponse, error) {
	userSession := s.verifySession(ctx)
	if userSession != nil {
		if userSession.SessionType == "temp" {
			return nil, grpc.Errorf(codes.FailedPrecondition, "Not allowed")
		}

		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - QuoteSend: %s\n", userSession.SessionId, string(prettyRequest))

		response, err := s.db.QuoteSend(in, userSession)

		if err == nil {
			grpclog.Printf("[%s] - QuoteSend DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - QuoteSend FAILED (%v)\n", userSession.SessionId, err)
		}
		return response, err
	}
	return nil, grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func (s *jinchiGRPCServer) Arigatou(ctx context.Context, in *pb.ArigatouRequest) (*pb.ArigatouResponse, error) {
	userSession := s.verifySession(ctx)
	if userSession != nil {
		if userSession.SessionType == "temp" {
			return nil, grpc.Errorf(codes.FailedPrecondition, "Not allowed")
		}
		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - Arigatou: %s\n", userSession.SessionId, string(prettyRequest))

		response, err := s.db.Arigatou(in, userSession)

		if err == nil {
			grpclog.Printf("[%s] - Arigatou DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - Arigatou FAILED (%v)\n", userSession.SessionId, err)
		}
		return response, err
	}
	return nil, grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func (s *jinchiGRPCServer) SearchQuotes(in *pb.SearchQuotesRequest, stream pb.JinchiService_SearchQuotesServer) error {
	userSession := s.verifySession(stream.Context())
	if userSession != nil {
		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - SearchQuotes: %s\n", userSession.SessionId, string(prettyRequest))

		err := s.db.SearchQuotes(in, stream, userSession)

		if err == nil {
			grpclog.Printf("[%s] - SearchQuotes DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - SearchQuotes FAILED (%v)\n", userSession.SessionId, err)
		}

		return err
	}
	return grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func (s *jinchiGRPCServer) TypeAhead(in *pb.QuoteTypeAheadRequest, stream pb.JinchiService_TypeAheadServer) error {
	prettyRequest, _ := json.Marshal(in)
	grpclog.Printf("TypeAhead: %s\n", string(prettyRequest))
	err := s.db.QuoteTypeAhead(in, stream)
	if err == nil {
		grpclog.Printf("TypeAhead DONE\n")
	} else {
		grpclog.Printf("TypeAhead: Failed (%v)", err)
	}
	return err
}

func (s *jinchiGRPCServer) UserList(ctx context.Context, in *pb.UserListMessage) (*pb.UserListResponse, error) {
	userSession := s.verifySession(ctx)
	if userSession != nil {
		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - UserList: %s\n", userSession.SessionId, string(prettyRequest))

		response, err := s.db.UserList(in, userSession)

		if err == nil {
			grpclog.Printf("[%s] - UserList DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - UserList FAILED (%v)\n", userSession.SessionId, err)
		}

		return response, err
	}
	return nil, nil
}

func (s *jinchiGRPCServer) QuoteToUserList(ctx context.Context, in *pb.QuoteToUserListMessage) (*pb.QuoteToUserListResponse, error) {
	userSession := s.verifySession(ctx)
	if userSession != nil {
		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - QuoteToUserList: %s\n", userSession.SessionId, string(prettyRequest))

		response, err := s.db.QuoteToUserList(in, userSession)

		if err == nil {
			grpclog.Printf("[%s] - QuoteToUserList DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - QuoteToUserList FAILED (%v)\n", userSession.SessionId, err)
		}

		return response, err
	}
	return nil, grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func (s *jinchiGRPCServer) QuoteUserList(in *pb.QuoteUserListMessage, stream pb.JinchiService_QuoteUserListServer) error {
	userSession := s.verifySession(stream.Context())
	if userSession != nil {
		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - QuoteUserList: %s\n", userSession.SessionId, string(prettyRequest))

		err := s.db.QuoteUserList(in, stream, userSession)

		if err == nil {
			grpclog.Printf("[%s] - QuoteUserList DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - QuoteUserList FAILED (%v)\n", userSession.SessionId, err)
		}

		return err
	}
	return grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func (s *jinchiGRPCServer) QuoteUserListExtended(in *pb.QuoteUserListMessage,
	stream pb.JinchiService_QuoteUserListExtendedServer) error {
	userSession := s.verifySession(stream.Context())
	if userSession != nil {
		prettyRequest, _ := json.Marshal(in)
		grpclog.Printf("[%s] - QuoteUserListExtended: %s\n", userSession.SessionId, string(prettyRequest))

		err := s.db.QuoteUserListExtended(in, stream, userSession)

		if err == nil {
			grpclog.Printf("[%s] - QuoteUserListExtended DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - QuoteUserListExtended FAILED (%v)\n", userSession.SessionId, err)
		}

		return err
	}
	return grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func (s *jinchiGRPCServer) UserProfile(ctx context.Context, in *pb.Empty) (*pb.UserProfileResponse, error) {
	userSession := s.verifySession(ctx)
	if userSession != nil {
		grpclog.Printf("[%s] - UserProfile\n", userSession.SessionId)

		response, err := s.db.UserProfile(userSession)

		if err == nil {
			grpclog.Printf("[%s] - UserProfile DONE\n", userSession.SessionId)
		} else {
			grpclog.Printf("[%s] - UserProfile Failed (%v)\n", userSession.SessionId, err)
		}

		return response, err
	}
	return nil, grpc.Errorf(codes.PermissionDenied, "Not logged in")
}

func newServer(db uint32, tls bool, env string) *jinchiGRPCServer {
	s := new(jinchiGRPCServer)
	s.db = repository.GetRepository(db, env)

	// Init server with any data that need loading from DB or files.
	if tls {
		creds, err := credentials.NewServerTLSFromFile("keys/server.pem", "keys/server.key")
		grpclog.Printf("TransportInfo: %v", creds.Info())
		if err != nil {
			log.Printf("%v", err)
		}
		s.options.tls = creds
	}

	s.options.compressor = grpc.NewGZIPCompressor()
	s.options.decompressor = grpc.NewGZIPDecompressor()

	return s
}

func (s *jinchiGRPCServer) serverOptions() *[]grpc.ServerOption {
	var options = []grpc.ServerOption{}

	if s.options.tls != nil {
		options = append(options, grpc.Creds(s.options.tls))
	}
	if s.options.maxConcurrentStreams != 0 {
		options = append(options, grpc.MaxConcurrentStreams(s.options.maxConcurrentStreams))
	}
	if s.options.compressor != nil {
		options = append(options, grpc.RPCCompressor(s.options.compressor))
	}
	if s.options.decompressor != nil {
		options = append(options, grpc.RPCDecompressor(s.options.decompressor))
	}

	return &options
}
