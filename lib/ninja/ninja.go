package ninja

import (
	"bytes"
	"math/rand"
	"strings"
	"time"

	"github.com/fiam/gounidecode/unidecode"
)

// Name takes a name and converts it to its Ninja equivalent
func Name(name string) string {
	var (
		// Letter slice that allows us to chkoose random letters
		letterRunes = []rune("abcdefghijklmnopqrstuvwxyz")

		// Map between latin letters and the ninja alphabet
		ninjaAlphabet = map[rune]string{
			'a': "ka",
			'b': "zu",
			'c': "mi",
			'd': "te",
			'e': "ku",
			'f': "lu",
			'g': "ji",
			'h': "ri",
			'i': "ki",
			'j': "zu",
			'k': "me",
			'l': "ta",
			'm': "rin",
			'n': "to",
			'o': "mo",
			'p': "no",
			'q': "ke",
			'r': "shi",
			's': "ari",
			't': "chi",
			'u': "do",
			'v': "ru",
			'w': "mei",
			'x': "na",
			'y': "fu",
			'z': "zi",
		}
	)

	var ninjaName bytes.Buffer

	for index, letter := range unidecode.Unidecode(strings.ToLower(name)) {
		ninjaName.WriteString(ninjaAlphabet[letter])
		if index >= 5 {
			break
		}
	}

	// If there are no letters in the user's name create one at random
	if ninjaName.Len() == 0 {
		rand.Seed(time.Now().UnixNano())
		for i := 0; i < 6; i++ {
			ninjaName.WriteString(ninjaAlphabet[letterRunes[rand.Intn(len(ninjaAlphabet))]])
		}
	}

	return ninjaName.String()
}
