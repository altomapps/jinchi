# Jinchi #

Server providing an API for Motiv8 Ninja to function

### Getting Started ###

To checkout use `git`:
```bash
git clone git@bitbucket.org:altomapps/jinchi.git
```
Build jinchi_motd with the following:
```bash
make <system>
```
The output of that command is the file jinchi_<version>.bsx
This self-extracting archive can be used as follows:
```bash
$ sudo ./jinchi_version>.bsx <environment>
```
Currently only 2 environments are supported "jinchi" for live and "jinchi_dev" for development.
The main difference between the 2 is the installation folder (which matches the environment name) and the port 8001 for live and 8002 for dev

### Configuration ###
In order to run jinchi_motd needs to be able to read certain configuration files:
```
config/
â”œâ”€â”€ config.json
â””â”€â”€ mongo.json
```
The provided versions should be ok for normal use.
#### config.json ####
Only one key is important for Jinchi in this config file and it is:
```json
{
    ...
    "api" : {
        "port" : "8002",
        ...
    }
    ...
}
```
This config will be deprecated soon as it has many things not really used by Jinchi but by the initial restful version of it.

#### mongo.json ####
```json
{
    "<environment>" :{
        "dial_info":{
            "addrs": ["<List of server addresses>"],
		    "database":  "<database name>",
		    "username":  "<username>",
		    "password":  "<password>",
		    "mechanism": "SCRAM-SHA-1"
        },
        "motd_collection": "<motd collection name>",
        "motd_quote_collection": "<motd_quote collection name>",
        "device_collection": "<device collection name>",
        "motd_collection_indexes":[],
        "motd_quote_collection_indexes":[]
    },
}
```
### SSL Certificates ###
Jinchi requires ssl/tls certificates to work properly. It looks for 2 files under the **keys** folder.

* **server.key**: The key generated during the creation for the certificate request on the server jinchi will run on.
* **server.pem**: The concatenated certificate of the host and the certificate authority with the host key being at the very beginning of the file.

### Sample usage ###
Manually by calling something like:
```bash
./jinchi -env <environment>
```
> Note: that <environemnt> is one of the defined in mongo.json if not provided it will default to dev so at least that should be defined in the config