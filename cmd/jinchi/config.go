package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/kardianos/osext"

	"google.golang.org/grpc/grpclog"
)

// Config contains the configuration of all the jinchi environments
type Config struct {
	Dev  *JinchiConfig `json:"dev"`
	Prod *JinchiConfig `json:"prod"`
}

// JinchiConfig is main config used by jinchi on a single environment.
type JinchiConfig struct {
	Port int `json:"port"`
}

func loadJinchiConfig(configFile string, env string) *JinchiConfig {
	var config Config
	execDir, err := osext.ExecutableFolder()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	file, e := ioutil.ReadFile(execDir + configFile)
	if e != nil {
		log.Fatalf("loadJinchiConfig: File error[%v]\n", e)
	}

	config.Dev = new(JinchiConfig)
	config.Prod = new(JinchiConfig)
	err = json.Unmarshal(file, &config)
	if err != nil {
		grpclog.Printf("loadJinchiConfig: %v\n", err)
	}

	switch *Env {
	case envProd:
		return config.Prod
	case envDev:
		return config.Dev
	default:
		return &JinchiConfig{Port: 8003}
	}
}
