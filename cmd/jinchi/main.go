package main

import (
	/* System Imports */
	"flag"
	"fmt"
	http2 "net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc/grpclog"

	/* Local Imports */

	"bitbucket.org/altomapps/jinchi/lib/repository"
	"bitbucket.org/altomapps/jinchi/lib/rpc"
)

var (
	// Version of Jinchi generated at build time
	Version string
	// BuildTime contains the date and time the binary was built
	BuildTime string
	// BuildBy contains the username and hostname of the building machine
	BuildBy string
)

// Command line parameters
var (
	Env     = flag.String("env", "prod", "Choose the environment the app will run on options: dev, prod")
	dbType  = flag.String("db", "mongo", "Choose the backend used by the api *Only valid for grpc* options: mongo, mock")
	tls     = flag.Bool("tls", false, "Enable or disable https support")
	ver     = flag.Bool("version", false, "Display version and exit")
	motd    = flag.Bool("motd", false, "Send the message of the day")
	quoteID = flag.String("quote", "", "QuoteID of the quote to be sent as the quote of the day for testing")
)

const (
	envProd = "prod"
	envDev  = "dev"
)

func main() {
	finish := make(chan bool)
	signals := make(chan os.Signal, 1)
	flag.Parse()

	// Look out for kill signals so that we can exit gracefully.
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	go killSignal(signals, finish)

	printVersion()
	quoteOfTheDay()
	jinchi(finish)

	// Wait for all threads to finish.
	<-finish

	cleanup()
}

// printVersion displays the version information of the application.
func printVersion() {
	if *ver {
		printVersionDetails()
		os.Exit(0)
	}
}

func printVersionDetails() {
	fmt.Println(" Jinchi Server")
	fmt.Println(" Version:\t", Version)
	fmt.Println(" Build time:\t", BuildTime)
	fmt.Println(" By:\t\t", BuildBy)
}

// quoteOfTheDay handles sending the quote of the day to all active users
func quoteOfTheDay() {
	if *motd {
		var quote *primitive.ObjectID

		quoteID, err := primitive.ObjectIDFromHex(*quoteID)
		if err == nil {
			// If we provide a quoteID force environment to be dev
			*Env = envDev
			fmt.Println(quoteID)
			quote = &quoteID
		}
		// Get an instance of the repository.
		repo := repository.GetRepository(getDBType(*dbType), *Env)

		// And send the quote of the day.
		repo.SendMotd(quote)
		os.Exit(0)
	}
}

// jinchi handles the main functionality of jinchi service
func jinchi(finish chan bool) {
	if *Env == envDev {
		runtime.SetBlockProfileRate(1)
	}

	go func() {
		grpclog.Println(http2.ListenAndServe("localhost:8080", nil))
	}()

	config := loadJinchiConfig("/config/config.json", *Env)

	fmt.Println("===========================================")
	fmt.Printf(" Starting ")
	printVersionDetails()
	fmt.Println("===========================================")
	grpclog.Println("Listening on port ", config.Port)

	startGPRCInterface(config.Port)
}

func startGPRCInterface(port int) {
	rpc.InitGRPCServer(port, getDBType(*dbType), *tls, *Env)
}

func getDBType(dbType string) uint32 {
	switch dbType {
	case "mongo":
		return repository.MongoDB
	default:
		return 0
	}
}

func cleanup() {
	rpc.StopGRPCServer()

	grpclog.Println("Exiting jinchi")
	grpclog.Println("=====================================================")
}

func killSignal(signals chan os.Signal, finish chan bool) {
	sig := <-signals

	grpclog.Printf("%v\n", sig)
	finish <- true
}
